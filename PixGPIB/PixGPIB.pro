CONFIG -= qt

TEMPLATE = lib

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES += PixGPIBDevice.cxx
SOURCES += PixGPIBError.cxx

INCLUDEPATH = ../include

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
}
win32 {
	LIBS += Gpib-32.obj
    DESTDIR = ../bin
	DLLDESTDIR = ../bin
	DEFINES += PIX_DLL_EXPORT
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
}
