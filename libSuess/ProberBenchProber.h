#ifndef _PROBERBENCH_PROBER_H 
#define _PROBERBENCH_PROBER_H 

#include "GenericProber.h"

class QTcpSocket;

namespace Suess {

  class DllExport ProberBenchProber : public GenericProber {
    public:
      ProberBenchProber(std::string host, int port, 
          std::string name, bool notify, int client_id);
      virtual ~ProberBenchProber();

  protected:
      void send(std::string);
      std::string recv();
      void send_name(std::string name);
      void send_notify(bool notify);

      std::unique_ptr<QTcpSocket> sock;
      const static int suess_tcp_timeout = 5000;
  };

}

#endif // _PROBERBENCH_PROBER_H 
