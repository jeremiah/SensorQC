#ifndef _VELOXRS232_PROBER_H 
#define _VELOXRS232_PROBER_H 

#include "GenericProber.h"
#include <ComTools.h>

namespace Suess {

  class DllExport VeloxRS232Prober : public GenericProber {
    public:
      VeloxRS232Prober(int port=COM1, int baudr=9600, int parity=P_NONE, 
		 int sbit=S_1BIT, int dbit=D_8BIT);
      ~VeloxRS232Prober();

  protected:
      void send(std::string);
      std::string recv();

      int m_port;
      const static int m_timeout = 1200;//=10 min.

  };

}

#endif // _VELOXRS232_PROBER_H 
