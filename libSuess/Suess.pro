TEMPLATE = lib

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES = GenericProber.cxx \
	  ProberBenchProber.cxx \
          VeloxRS232Prober.cxx

DESTDIR = .
INCLUDEPATH += ../include ../PixRS232 ../PixGPIB

unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        QMAKE_LFLAGS  +=  -L../PixGPIB -lPixGPIB -L../PixRS232 -lPixRS232
        QMAKE_RPATHDIR += ../PixGPIB ../PixRS232
}

win32{
  DLLDESTDIR = ..\bin
  DESTDIR = ..\bin
  CONFIG += dll
  DEFINES += WIN32 PIX_DLL_EXPORT
  QMAKE_LFLAGS_RELEASE = delayimp.lib
  QMAKE_LFLAGS_WINDOWS += /LIBPATH:../PixGPIB /LIBPATH:../bin
  LIBS += PixGPIB.lib GPIB-32.obj PixRS232.lib
}

QT = network core
