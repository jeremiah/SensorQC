#include "GenericProber.h"

/////////////////////////////////////////////////////////////////////
// OpenSuessPixProber.cxx
/////////////////////////////////////////////////////////////////////

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdlib.h>
#include "GenericProber.h"

using namespace Suess;

const bool OpenSuessPixProber_Debug = false;

GenericProber::GenericProber() : m_client_id(0) {
}

GenericProber::~GenericProber() {
}

std::string GenericProber::clientCommand(std::string cmd, std::string data)
{
  m_mutex.lock();
  std::stringstream buf;
  if(p.checkId){
    buf << "Cmd="
	<< m_client_id
	<< p.delimW;
  }
  buf << cmd;
  if(data!=""){
    buf << p.delimW
	<< data;
  }
  buf << p.eol;

  send(buf.str());
  std::string answer = recv();
  m_mutex.unlock();
  return answer;
}

void GenericProber::MoveChuckContact(float velocity)
{
  std::stringstream data;
  std::string cmd = "MoveChuckContact";
  if (velocity > 0)
  {
    if (velocity > 1)
    {
      velocity = 1;
    }
    data << int(velocity * 100);
  }

  Response r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
}
/**
 * \brief move to an arbitrary position or an arbitray distance
 * \param x			X position or distance
 * \param Y			Y position or distance
 * \param PosRef	refer to... 'H'=Home, 'Z'=Zero, 'C'=Center, 'R'=current ...position
 * \param Unit		Size of steps: 'Y'=Micron, 'I'= Mils, 'X'=Index, 'J'=Jog
 * \param velocity	speed of movement in percent
 * \param comp		comp. level: 'D'=default, 'N'=None, 'P'=Prober, 'T'=Technology
 */

void GenericProber::MoveChuck(float x,float y, char PosRef, char Unit, 
							 float velocity, char comp){
  std::stringstream data;
  std::string cmd = "MoveChuck";
      data<<std::fixed<<std::showpoint<<std::setprecision(2);
      data << x;
	  data << " ";
      data << y;
	  data << " ";

	if (PosRef=='H'||PosRef=='Z'||PosRef=='C'||PosRef=='R')
    {
      data << PosRef; 
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong reference point.");

	if (Unit=='Y'||Unit=='I'||Unit=='X'||Unit=='J')
    {
      data << Unit;  
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong unit.");
   data<<std::resetiosflags(std::ios::fixed )<<std::resetiosflags(std::ios::showpoint )<<std::setprecision(0);
	if (velocity > 1||velocity<0)
    {
      velocity = 1;
    } 
	  data << int(velocity * 100);
	  data << " ";

    if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
    {
      data << comp;
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong comp. level.");

    Response r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  
}

void GenericProber::MoveChuckWait(float x,float y, char PosRef, char Unit, 
				  float velocity, char comp){

  MoveChuck(x,y,PosRef,Unit,velocity,comp);
  bool isMoving=true;
  int timeout=0;
  const int maxTimeout=50;
  while(isMoving && timeout<maxTimeout){
    ReadChuckStatusResponse s(ReadChuckStatus());
    isMoving = s.r_move>0;
    timeout++;
  }
  if(timeout>=maxTimeout)
    throw Exception("Suess::GenericProber::MoveChuckWait: timeout waiting for chuck to stop moving.");
}

ReadChuckPositionResponse GenericProber::ReadChuckPosition(char Unit, 
	char PosRef, char comp)
{

  std::stringstream data;
  std::string cmd = "ReadChuckPosition";
  if (Unit=='Y'||Unit=='I'||Unit=='X')
    {
      data << Unit;  
	  data << " ";
    } 
  else throw Exception("Suess::GenericProber::ReadChuckPosition: Wrong unit.");
  
  if (PosRef=='H'||PosRef=='Z'||PosRef=='C')
    {
      data << PosRef; 
	  data << " ";
    }
  else throw Exception("Suess::GenericProber::ReadChuckPosition: Wrong reference point.");

  if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
    {
      data << comp;
    }
  else throw Exception("Suess::GenericProber::ReadChuckPosition: Wrong comp. Mode.");

  ReadChuckPositionResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

ReadChuckStatusResponse GenericProber::ReadChuckStatus() {
  std::string cmd = "ReadChuckStatus";
  ReadChuckStatusResponse r(clientCommand(cmd, ""),p);
  r.validate(m_client_id);
  return r;
}

void GenericProber::MoveChuckZ(float z, char PosRef, char Unit, 
							 float velocity, char comp){
  std::stringstream data;
  std::string cmd = "MoveChuckZ";
	data<<std::fixed<<std::showpoint<<std::setprecision(2);
      data << z;
	  data << " ";

	if (PosRef=='H'||PosRef=='Z'||PosRef=='R')
    {
      data << PosRef; 
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong reference point.");

	if (Unit=='Y'||Unit=='I'||Unit=='J')
    {
      data << Unit;  
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong unit.");
 data<<std::resetiosflags(std::ios::fixed )<<std::resetiosflags(std::ios::showpoint )<<std::setprecision(0);
   if (velocity<0||velocity>1)
    {
      velocity=1;
    } 
    data << Unit;  
	  data << " ";

    if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
    {
      data << comp;
    } 
	else throw Exception("Suess::GenericProber::MoveChuck: Wrong comp. level.");

    Response r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
}

void GenericProber::MoveChuckZWait(float z, char PosRef, char Unit, 
				   float velocity, char comp){
  MoveChuck(z, PosRef, Unit, velocity, comp);

  bool isMoving=true;
  int timeout=0;
  const int maxTimeout=50;
  while(isMoving && timeout<maxTimeout){
    ReadChuckStatusResponse s(ReadChuckStatus());
    isMoving = s.r_move>0;
    timeout++;
  }
  if(timeout>=maxTimeout)
    throw Exception("Suess::GenericProber::MoveChuckWait: timeout waiting for chuck to stop moving.");
}

void GenericProber::MoveScope(float x, float y, char PosRef, char Unit, float velocity, char comp){
  std::stringstream data;
  std::string cmd = "MoveScope";
      data<<std::fixed<<std::showpoint<<std::setprecision(2);
      data << x;
	  data << " ";
      data << y;
	  data << " ";

	if (PosRef=='H'||PosRef=='Z'||PosRef=='C'||PosRef=='R')
    {
      data << PosRef; 
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveScope: Wrong reference point.");

	if (Unit=='Y'||Unit=='I'||Unit=='X'||Unit=='J')
    {
      data << Unit;  
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::MoveScope: Wrong unit.");
   data<<std::resetiosflags(std::ios::fixed )<<std::resetiosflags(std::ios::showpoint )<<std::setprecision(0);
	if (velocity > 1||velocity<0)
    {
      velocity = 1;
    } 
	  data << int(velocity * 100);
	  data << " ";

    if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
    {
      data << comp;
    } 
	else throw Exception("Suess::GenericProber::MoveScope: Wrong comp. level.");

    Response r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  
}
ReadScopePositionResponse GenericProber::ReadScopePosition(char Unit, char PosRef, char comp){
	std::stringstream data;
	std::string cmd = "ReadScopePosition";

	if (Unit=='Y'||Unit=='I'||Unit=='X')
    {
      data << Unit;  
	  data << " ";
    } 
	else throw Exception("Suess::GenericProber::ReadScopePosition: Wrong unit.");

	if (PosRef=='H'||PosRef=='Z'||PosRef=='C')
    {
      data << PosRef; 
	  data << " ";
    }
	else throw Exception("Suess::GenericProber::ReadScopePosition: Wrong reference point.");

	if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
    {
      data << comp;
    }
	else throw Exception("Suess::GenericProber::ReadScopePosition: Wrong comp. Mode.");

	ReadScopePositionResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

void GenericProber::MoveScopeZ(float /*z*/, char /*PosRef*/, char /*Unit*/,float /*velocity*/, char /*comp*/){
  throw Exception("Suess::GenericProber::MoveScopeZ: Currently disabled until cause for malfunctioning is identified.");
//  std::stringstream data;
//   std::string cmd = "MoveScopeZ";
//       data<<std::fixed<<std::showpoint<<std::setprecision(2);
//       data << z;
// 	  data << " ";

// 	if (PosRef=='H'||PosRef=='Z'||PosRef=='R')
//     {
//       data << PosRef; 
// 	  data << " ";
//     } 
// 	else throw Exception("Suess::GenericProber::MoveScopeZ: Wrong reference point.");

// 	if (Unit=='Y'||Unit=='I'||Unit=='J')
//     {
//       data << Unit;  
// 	  data << " ";
//     } 
// 	else throw Exception("Suess::GenericProber::MoveScopeZ: Wrong unit.");

//    if (velocity<0||velocity>1)
//     {
//       velocity=1;
//     } 
//     data << Unit;  
// 	  data << " ";

//     if (comp=='D'||comp=='N'||comp=='P'||comp=='T')
//     {
//       data << comp;
//     } 
// 	else throw Exception("Suess::GenericProber::MoveScope: Wrong comp. level.");

//   Response r(clientCommand(cmd, data.str()));

//   r.validate(m_client_id);
}

void GenericProber::MoveChuckSeparation(float velocity)
{
  std::stringstream data;
  std::string cmd = "MoveChuckSeparation";
  if (velocity > 0)
  {
    if (velocity > 1)
    {
      velocity = 1;
    }
    data << int(velocity * 100);
  }

  Response r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
}
        
StepDieResponse GenericProber::StepFirstDie(bool ClearBins, bool RecalcRoute)
{
  std::stringstream data;
  std::string cmd = "StepFirstDie";
  data << ClearBins;
  data << " ";
  data << RecalcRoute;

  StepDieResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

StepDieResponse GenericProber::StepNextDie(
    int column, int row, int subdie)
{
  std::stringstream data;
  std::string cmd = "StepNextDie";

  if ((column != -1) && (row != -1) && (subdie != -1))
  {
    data << column;
    data << " ";
    data << row;
    data << " ";
    data << subdie;
  }

  StepDieResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

ReadMapPositionResponse GenericProber::ReadMapPosition(
    int pos, char from_pos)
{
  std::stringstream data;
  std::string cmd = "ReadMapPosition";
  data << pos;
  data << " ";
  data << from_pos;

  ReadMapPositionResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

ReadChuckActualTempResponse GenericProber::ReadChuckActualTemp()
{
  std::stringstream data;
  std::string cmd = "GetHeaterTemp";
  data << " C";

  ReadChuckActualTempResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

ReadChuckSetTempResponse GenericProber::ReadChuckSetTemp()
{
  std::stringstream data;
  std::string cmd = "GetTargetTemp";
  data << " C";

  ReadChuckSetTempResponse r(clientCommand(cmd, data.str()),p);

  r.validate(m_client_id);
  return r;
}

Response::Parset::Parset(){
  eol     = "\r\n";
  delimR  = ':';
  delimW  = ':';
  checkId = true;
  haveErrorInfo = true;
}
Response::Parset::Parset(const Parset &p)
{
  eol = p.eol;
  delimR = p.delimR;
  delimW = p.delimW;
  checkId = p.checkId;
  haveErrorInfo = p.haveErrorInfo;
}

Response::Response(const Response & r)
{
  client_id = r.client_id;
  error_code = r.error_code;
  return_data = r.return_data;
  p = r.p;
}
        
Response::Response(std::string r, Parset p_in) : p(p_in)
{
  if (OpenSuessPixProber_Debug)
  {
    std::cerr << "Response::Response(" << r << ")" << std::endl;
  }
  
  if ((p.checkId && r.substr(0, 4) != "Rsp=") || (!p.checkId && r.substr(1,1) != ":"))
  {
    throw Exception("Response::Response: Parser error: \"" + r + "\"");
  }
  std::istringstream ss(p.checkId?r.substr(4):r);
  std::string x;

  if (ss.eof())
  {
    throw Exception("Response::Response: Parser error: \"" + r + "\"");
  }
  std::getline(ss, x, p.delimR);
  client_id = p.checkId?atoi(x.data()):0;
  
  if(p.haveErrorInfo) {
    if (ss.eof())
      {
	throw Exception("Response::Response: Parser error: \"" + r + "\"");
      }
    std::getline(ss, x, p.delimR);
    error_code = atoi(x.data());
  } else
    error_code = 0;

  if (ss.eof())
  {
    throw Exception("Response::Response: Parser error: \"" + r + "\"");
  }
  std::getline(ss, return_data, p.delimR);
  data_iss.reset(new std::istringstream(return_data));
}
        
std::string Response::parseString()
{
  if (data_iss->eof())
  {
    throw Exception("Response::Response: Data parser error: \"" + return_data 
        + "\"");
  }
  std::string r;
  std::getline(*data_iss, r, ' ');
  return r;
}

int Response::parseInt()
{
  return atoi(parseString().data());
}
        
float Response::parseFloat()
{
  return atof(parseString().data());
}
        
void Response::validate(int client_id)
{
  if (OpenSuessPixProber_Debug)
  {
    dump();
  }

  if (client_id != this->client_id)
  {
    throw Exception("Response::validate: Client id mismatch.");
  }
}

void Response::dump()
{
  std::cerr << "Response: client id = " << client_id << ", error_code = " << error_code << ", data = " << return_data << std::endl;
}

StepDieResponse::StepDieResponse(std::string d, Parset p): Response(d, p),
  r_column(0), r_row(0), r_subdie(0), r_totalsubdies(0)
{
  if (error_code == 0)
  {
    r_column       = parseInt();
    r_row          = parseInt();
    r_subdie       = parseInt();
    r_totalsubdies = parseInt();
  }
}
        
StepDieResponse::StepDieResponse(const StepDieResponse & r):
  Response(r)
{
  r_column = r.r_column;
  r_row    = r.r_row;
  r_subdie = r.r_subdie;
  r_totalsubdies = r.r_totalsubdies;
}

ReadChuckPositionResponse::ReadChuckPositionResponse(std::string d, Parset p): Response(d, p), //???
  r_x(0), r_y(0), r_z(0)
{
  if (error_code == 0)
  {
    r_x = parseFloat();
    r_y = parseFloat();
    r_z = parseFloat();
  }
}
        
ReadChuckStatusResponse::ReadChuckStatusResponse(std::string d, Parset p) : Response(d,p) ,
  r_move(0), r_limit(0), r_height("")
{
  parseInt();//init.
  parseInt();//mode
  r_limit = parseInt();
  r_move = parseInt();
  parseString(); // compMode
  parseInt();//vacuum
  r_height = parseString();
}
ReadChuckStatusResponse::ReadChuckStatusResponse(const ReadChuckStatusResponse & r) : Response(r)
{
  r_move = r.r_move;
  r_limit = r.r_limit;
  r_height = r.r_height;
}

ReadChuckPositionResponse::ReadChuckPositionResponse(const ReadChuckPositionResponse & r):
  Response(r)
{
  r_x = r.r_x;
  r_y = r.r_y;
  r_z = r.r_z;

}

ReadScopePositionResponse::ReadScopePositionResponse(std::string d, Parset p): Response(d,p), //???
  r_x(0), r_y(0), r_z(0)
{
  if (error_code == 0)
  {
    r_x = parseFloat();
    r_y = parseFloat();
    r_z = parseFloat();
  }
}
        
ReadScopePositionResponse::ReadScopePositionResponse(const ReadScopePositionResponse & r):
  Response(r)
{
  r_x = r.r_x;
  r_y = r.r_y;
  r_z = r.r_z;

}

ReadMapPositionResponse::ReadMapPositionResponse(std::string d, Parset p): 
  Response(d,p)
{
  r_column = 0;
  r_row = 0;
  r_x_position = 0;
  r_y_position = 0;
  r_current_subdie = 0;
  r_total_subdies = 0;
  r_current_die = 0;
  r_total_dies = 0;
  r_current_cluster = 0;
  r_total_clusters = 0;

  if (error_code == 0)
  {
    r_column = parseInt();
    r_row = parseInt();
    r_x_position = parseFloat();
    r_y_position = parseFloat();
    r_current_subdie = parseInt();
    r_total_subdies = parseInt();
    r_current_die = parseInt();
    r_total_dies = parseInt();
    r_current_cluster = parseInt();
    r_total_clusters = parseInt();
  }
}
ReadChuckActualTempResponse::ReadChuckActualTempResponse(std::string d, Parset p): 
  Response(d,p)
{
  r_temp = 0.;
  r_unit = "?";
  r_status = 0;
  r_sstatus = "???";

  m_statusOpts[0] = "unknown"; // (->ATT not connected/off)
  m_statusOpts[1] = "heating";
  m_statusOpts[2] = "cooling";
  m_statusOpts[4] = "at set T";
  m_statusOpts[8] = "else";

  if (error_code == 0)
  {
    r_temp = parseFloat();
    r_unit = parseString();
    r_status = parseInt();
    if(m_statusOpts.find(r_status)!=m_statusOpts.end()) r_sstatus = m_statusOpts[r_status];
  }
}
ReadChuckActualTempResponse::ReadChuckActualTempResponse(const ReadChuckActualTempResponse & r):
  Response(r) 
{
  r_temp = r.r_temp;
  r_unit = r.r_unit;
  r_status = r.r_status;
  r_sstatus = r.r_sstatus;

  m_statusOpts[0] = "unknown"; // (->ATT not connected/off)
  m_statusOpts[1] = "heating";
  m_statusOpts[2] = "cooling";
  m_statusOpts[4] = "at set T";
  m_statusOpts[8] = "else";
}

ReadChuckSetTempResponse::ReadChuckSetTempResponse(std::string d, Parset p): 
  Response(d,p)
{
  r_temp = 0.;
  r_unit = "?";

  if (error_code == 0)
  {
    r_temp = parseFloat();
    r_unit = parseString();
  }
}
ReadChuckSetTempResponse::ReadChuckSetTempResponse(const ReadChuckSetTempResponse & r):
  Response(r) 
{
  r_temp = r.r_temp;
  r_unit = r.r_unit;
}

void ReadChuckActualTempResponse::dump(){
  Response::dump();
  std::cerr << " + GetHeaterTemp: " 
    << " r_temp = " << r_temp << r_unit << ", "
	    << " r_sstatus = " << r_sstatus << std::endl;
}
void ReadChuckSetTempResponse::dump(){
  Response::dump();
  std::cerr << " + GetTargetTemp: " 
	    << " r_temp = " << r_temp << r_unit << std::endl;
}

void ReadMapPositionResponse::dump()
{
  Response::dump();
  std::cerr << " + StepDieResponse: " 
    << " r_column = " << r_column << ", "
    << " r_row = " << r_row << ", "
    << " r_x_position = " << r_x_position << ", "
    << " r_y_position = " << r_y_position << ", "
    << " r_current_subdie = " << r_current_subdie << ", "
    << " r_total_subdies = " << r_total_subdies << ", "
    << " r_current_die = " << r_current_die << ", "
    << " r_total_dies = " << r_total_dies << ", "
    << " r_current_cluster = " << r_current_cluster << ", "
    << " r_total_clusters = " << r_total_clusters << std::endl;
}

ReadMapPositionResponse::ReadMapPositionResponse(
    const ReadMapPositionResponse & r):
  Response(r)
{
  r_column          = r.r_column;
  r_row             = r.r_row;
  r_x_position      = r.r_x_position;
  r_y_position      = r.r_y_position;
  r_current_subdie  = r.r_current_subdie;
  r_total_subdies   = r.r_total_subdies;
  r_current_die     = r.r_current_die;
  r_total_dies      = r.r_total_dies;
  r_current_cluster = r.r_current_cluster;
  r_total_clusters  = r.r_total_clusters;
}

void StepDieResponse::dump()
{
  Response::dump();
  std::cerr << " + StepDieResponse: " 
    << " r_column = " << r_column << ", "
    << " r_row = "    << r_row << ", "
    << " r_subdie = " << r_subdie << ", "
    << " r_totalsubdies = " << r_totalsubdies << std::endl;
}

void ReadChuckPositionResponse::dump()
{
  Response::dump();
  std::cerr << " + ReadChuckPositionResponse: " 
    << " r_x = " << r_x << ", "
    << " r_y = " << r_y << ", "
    << " r_z = " << r_z << std::endl;
}
void ReadScopePositionResponse::dump()
{
  Response::dump();
  std::cerr << " + ReadScopePositionResponse: " 
    << " r_x = " << r_x << ", "
    << " r_y = " << r_y << ", "
    << " r_z = " << r_z << std::endl;
}


/* to do: thermo-chuck set command
HeatChuck [x.y] C
-> x.y C
*/
