
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <string.h>

#include "ComTools.h"
#include "VeloxRS232Prober.h"

int main(int /*argc*/, char **/*argv*/)
{
  try{

    Suess::VeloxRS232Prober myPA300(COM1);
    std::string status = myPA300.clientCommand("GetStatus","");
    std::cout << "Status: " << status << std::endl;
    std::cout << "initial position: " << std::endl;
    Suess::ReadChuckPositionResponse pos1(myPA300.ReadChuckPosition('Y', 'H', 'D'));
    pos1.dump();
    Suess::ReadChuckStatusResponse s(myPA300.ReadChuckStatus());
    if(s.r_height!="S"){
      std::cout << "Not in separate mode, will request now" << std::endl;
      myPA300.MoveChuckSeparation();
    }

    std::cout << "moving to (500,0): " << std::endl;
    myPA300.MoveChuckWait(500.,0.);
    Suess::ReadChuckPositionResponse pos2(myPA300.ReadChuckPosition('Y', 'H', 'D'));
    pos2.dump();
    std::cout << "moving to (500,500): " << std::endl;
    myPA300.MoveChuckWait(500., 500.);
    Suess::ReadChuckPositionResponse pos3(myPA300.ReadChuckPosition('Y', 'H', 'D'));
    pos3.dump();
    std::cout << "moving to (0,0): " << std::endl;
    myPA300.MoveChuckWait(0., 0.);

  }catch(Suess::Exception e){
    std::cerr << "Suess exception: " << e.text<< std::endl;
  }catch(...){
    std::cerr << "Unknown exception while executing" << std::endl;
  }
  return 0;
}
