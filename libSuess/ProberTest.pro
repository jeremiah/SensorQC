CONFIG -= qt

TEMPLATE = app

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES += ProberTest.cxx

INCLUDEPATH += ../include ../PixGPIB ../PixRS232
unix {
    DESTDIR = .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX
	LIBS += -L. -lSuess -L../PixRS232 -lPixRS232 -L../PixGPIB -lPixGPIB
	QMAKE_RPATHDIR += . ../PixRS232 ../PixGPIB
}
win32 {
    DESTDIR = ../bin
    CONFIG += console
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
	DEFINES += "_CRT_SECURE_NO_WARNINGS" 
    DEFINES += __VISUALC__ 
	QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../PixGPIB /LIBPATH:../bin
    LIBS += PixGPIB.lib GPIB-32.obj PixRS232.lib Suess.lib
}

