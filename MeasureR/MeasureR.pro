TEMPLATE = app

CONFIG -= qt
unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

INCLUDEPATH += . ../PixGPIB ../include

SOURCES += MeasureR.cxx 

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        INCLUDEPATH += $${system(root-config --incdir)}
	INCLUDEPATH += /usr/include/python2.7
	LIBS += -L /usr/local/lib/python2.7 -lpython2.7
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB
        QMAKE_RPATHDIR += ../PixGPIB
}
win32 {
    DESTDIR =  ../bin
    CONFIG += console
	DEFINES += WIN32 
	DEFINES += _WINDOWS
	DEFINES += _MBCS 
	QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
	INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_CONSOLE += /LIBPATH:../PixGPIB /LIBPATH:../bin /LIBPATH:$(ROOTSYS)/lib
    LIBS += PixGPIB.lib GPIB-32.obj
    LIBS += libCore.lib
    LIBS += libCint.lib 
    LIBS += libRIO.lib 
    LIBS += libNet.lib 
    LIBS += libHist.lib 
    LIBS += libGraf.lib 
    LIBS += libGraf3d.lib 
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
}
