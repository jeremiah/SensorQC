/* ******************************
   * MeasureR 1.0                 *
   * steffen.korn@cern.ch    	  *
   ****************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <ctime>
#include <thread>
#include <chrono>
#include <math.h>
#include "upsleep.h"
#include <iomanip>
#include <Python.h>

#include "PixGPIBDevice.h"

std::string LookUp(int i);

int main(int argc, const char* argv[]){
  
	char PYTHON_SERIAL_FILE_NAME[] = "Connect_Arduino.py";

	Py_Initialize();
	PyObject* PyFileObject = PyFile_FromString(PYTHON_SERIAL_FILE_NAME, "r");
	PyRun_SimpleFileEx(PyFile_AsFile(PyFileObject), PYTHON_SERIAL_FILE_NAME, 1);

	PixGPIBDevice myDevice(0, 18, 1, 0);
	std::cout << "Device is " << myDevice.getDescription() << std::endl;
	std::cout<<"Device has "<<myDevice.getDeviceNumberChannels()<<"channels"<<std::endl;
	
	// open fstream
	std::ofstream outTxtFile;

	//open arduino device file (linux)
        std::ofstream arduino;

	//write to it
	int i = 0;
	bool first = true;
	double R;
	for(int j=0; j<1;j++) {
		i=1;
		while(i<=337){

			std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
			std::time_t scan_time = std::chrono::system_clock::to_time_t(start);
			std::tm* timeinfo;
			char timeChar [80];
			std::time(&scan_time);
			timeinfo = std::localtime(&scan_time);
			std::strftime(timeChar,80,"%Y-%m-%d %H:%M:%S",timeinfo); //custom date format
			arduino.open( "/dev/ttyACM0");
			arduino << LookUp(i);
			myDevice.measureResistances(1., true, 0);
			R = myDevice.getResistance(0);
			std::cout << i << "\t" <<  timeChar << "\t" <<  R << "\n";

			arduino.close();
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			if(first == true) {
			  first = false;
			}
			else {
			outTxtFile.open("Barcelona_3_12-3_0_Cycles.dat", std::ofstream::out | std::ofstream::app);
			outTxtFile <<  timeChar  << "\t" << R <<"\n";
			outTxtFile.close();
			i++;
			}
		}

		std::cout << j << std::endl;
	        outTxtFile.open("Barcelona_3_12-3_0_Cycles.dat", std::ofstream::out | std::ofstream::app);
		outTxtFile << "\n";
		outTxtFile.close();
	}

	return 0;
}

std::string LookUp(int i){
  switch ( i )
    {
    case 1:
      return "28 28 42 45;";

    case 2:
      return "28 27 42 45;";

    case 3:
      return "27 27 42 45;";

    case 4:
      return "27 26 42 45;";

    case 5:
      return "26 26 42 45;";

    case 6:
      return "26 25 42 45;";

    case 7:
      return "25 25 42 45;";

    case 8:
      return "25 24 42 45;";

    case 9:
      return "24 24 42 45;";

    case 10:
      return "24 23 42 45;";

    case 11:
      return "23 23 42 45;";

    case 12:
      return "23 22 42 45;";

    case 13:
      return "22 22 42 45;";

    case 14:
      return "22 21 42 45;";

    case 15:
      return "21 21 42 45;";

    case 16:
      return "21 20 42 45;";

    case 17:
      return "20 20 42 45;";

    case 18:
      return "20 0 42 33;";

    case 19:
      return "0 0 30 33;";

    case 20:
      return "0 1 30 33;";

    case 21:
      return "1 1 30 33;";

    case 22:
      return "1 2 30 33;";

    case 23:
      return "2 2 30 33;";

    case 24:
      return "2 3 30 33;";

    case 25:
      return "3 3 30 33;";

    case 26:
      return "3 4 30 33;";

    case 27:
      return "4 4 30 33;";

    case 28:
      return "4 5 30 33;";

    case 29:
      return "5 5 30 33;";

    case 30:
      return "5 6 30 33;";

    case 31:
      return "6 6 30 33;";

    case 32:
      return "6 7 30 33;";

    case 33:
      return "7 7 30 33;";

    case 34:
      return "7 8 30 33;";

    case 35:
      return "8 8 30 33;";

    case 36:
      return "8 9 30 33;";

    case 37:
      return "9 9 30 33;";

    case 38:
      return "9 10 30 33;";

    case 39:
      return "10 10 30 33;";

    case 40:
      return "10 11 30 33;";

    case 41:
      return "11 11 30 33;";

    case 42:
      return "11 12 30 33;";

    case 43:
      return "12 12 30 33;";

    case 44:
      return "12 13 30 33;";

    case 45:
      return "13 13 30 33;";

    case 46:
      return "13 14 30 33;";

    case 47:
      return "14 14 30 33;";

    case 48:
      return "14 15 30 33;";

    case 49:
      return "15 15 30 33;";

    case 50:
      return "15 29 30 45;";

    case 51:
      return "29 29 42 45;";

    case 52:
      return "29 30 42 45;";

    case 53:
      return "30 30 42 45;";

    case 54:
      return "30 31 42 45;";

    case 55:
      return "31 31 42 45;";

    case 56:
      return "31 15 42 45;";

    case 57:
      return "15 15 42 45;";

    case 58:
      return "15 14 42 45;";

    case 59:
      return "14 14 42 45;";

    case 60:
      return "14 13 42 45;";

    case 61:
      return "13 13 42 45;";

    case 62:
      return "13 12 42 45;";

    case 63:
      return "12 12 42 45;";

    case 64:
      return "12 11 42 45;";

    case 65:
      return "11 11 42 45;";

    case 66:
      return "11 10 42 45;";

    case 67:
      return "10 10 42 45;";

    case 68:
      return "10 16 42 33;";

    case 69:
      return "16 16 30 33;";

    case 70:
      return "16 17 30 33;";

    case 71:
      return "17 17 30 33;";

    case 72:
      return "17 18 30 33;";

    case 73:
      return "18 18 30 33;";

    case 74:
      return "18 19 30 33;";

    case 75:
      return "19 19 30 33;";

    case 76:
      return "19 20 30 33;";

    case 77:
      return "20 20 30 33;";

    case 78:
      return "20 21 30 33;";

    case 79:
      return "21 21 30 33;";

    case 80:
      return "21 22 30 33;";

    case 81:
      return "22 22 30 33;";

    case 82:
      return "22 23 30 33;";

    case 83:
      return "23 23 30 33;";

    case 84:
      return "23 24 30 33;";

    case 85:
      return "24 24 30 33;";

    case 86:
      return "24 25 30 33;";

    case 87:
      return "25 25 30 33;";

    case 88:
      return "25 26 30 33;";

    case 89:
      return "26 26 30 33;";

    case 90:
      return "26 27 30 33;";

    case 91:
      return "27 27 30 33;";

    case 92:
      return "27 28 30 33;";

    case 93:
      return "28 28 30 33;";

    case 94:
      return "28 29 30 33;";

    case 95:
      return "29 29 30 33;";

    case 96:
      return "29 30 30 33;";

    case 97:
      return "30 30 30 33;";

    case 98:
      return "30 31 30 33;";

    case 99:
      return "31 31 30 33;";

    case 100:
      return "31 5 30 49;";

    case 101:
      return "5 5 46 49;";

    case 102:
      return "5 6 46 49;";

    case 103:
      return "6 6 46 49;";

    case 104:
      return "6 7 46 49;";

    case 105:
      return "7 7 46 49;";

    case 106:
      return "7 8 46 49;";

    case 107:
      return "8 8 46 49;";

    case 108:
      return "8 9 46 49;";

    case 109:
      return "9 9 46 49;";

    case 110:
      return "9 10 46 49;";

    case 111:
      return "10 10 46 49;";

    case 112:
      return "10 11 46 49;";

    case 113:
      return "11 11 46 49;";

    case 114:
      return "11 12 46 49;";

    case 115:
      return "12 12 46 49;";

    case 116:
      return "12 13 46 49;";

    case 117:
      return "13 13 46 49;";

    case 118:
      return "13 0 46 37;";

    case 119:
      return "0 0 34 37;";

    case 120:
      return "0 1 34 37;";

    case 121:
      return "1 1 34 37;";

    case 122:
      return "1 2 34 37;";

    case 123:
      return "2 2 34 37;";

    case 124:
      return "2 3 34 37;";

    case 125:
      return "3 3 34 37;";

    case 126:
      return "3 4 34 37;";

    case 127:
      return "4 4 34 37;";

    case 128:
      return "4 5 34 37;";

    case 129:
      return "5 5 34 37;";

    case 130:
      return "5 6 34 37;";

    case 131:
      return "6 6 34 37;";

    case 132:
      return "6 7 34 37;";

    case 133:
      return "7 7 34 37;";

    case 134:
      return "7 8 34 37;";

    case 135:
      return "8 8 34 37;";

    case 136:
      return "8 9 34 37;";

    case 137:
      return "9 9 34 37;";

    case 138:
      return "9 10 34 37;";

    case 139:
      return "10 10 34 37;";

    case 140:
      return "10 11 34 37;";

    case 141:
      return "11 11 34 37;";

    case 142:
      return "11 12 34 37;";

    case 143:
      return "12 12 34 37;";

    case 144:
      return "12 13 34 37;";

    case 145:
      return "13 13 34 37;";

    case 146:
      return "13 14 34 37;";

    case 147:
      return "14 14 34 37;";

    case 148:
      return "14 15 34 37;";

    case 149:
      return "15 15 34 37;";

    case 150:
      return "15 4 34 49;";

    case 151:
      return "4 4 46 49;";

    case 152:
      return "4 3 46 49;";

    case 153:
      return "3 3 46 49;";

    case 154:
      return "3 2 46 49;";

    case 155:
      return "2 2 46 49;";

    case 156:
      return "2 1 46 49;";

    case 157:
      return "1 1 46 49;";

    case 158:
      return "1 0 46 49;";

    case 159:
      return "0 0 46 49;";

    case 160:
      return "0 16 46 45;";

    case 161:
      return "16 16 42 45;";

    case 162:
      return "16 17 42 45;";

    case 163:
      return "17 17 42 45;";

    case 164:
      return "17 18 42 45;";

    case 165:
      return "18 18 42 45;";

    case 166:
      return "18 19 42 45;";

    case 167:
      return "19 19 42 45;";

    case 168:
      return "19 16 42 37;";

    case 169:
      return "16 16 34 37;";

    case 170:
      return "16 17 34 37;";

    case 171:
      return "17 17 34 37;";

    case 172:
      return "17 18 34 37;";

    case 173:
      return "18 18 34 37;";

    case 174:
      return "18 19 34 37;";

    case 175:
      return "19 19 34 37;";

    case 176:
      return "19 20 34 37;";

    case 177:
      return "20 20 34 37;";

    case 178:
      return "20 21 34 37;";

    case 179:
      return "21 21 34 37;";

    case 180:
      return "21 22 34 37;";

    case 181:
      return "22 22 34 37;";

    case 182:
      return "22 23 34 37;";

    case 183:
      return "23 23 34 37;";

    case 184:
      return "23 24 34 37;";

    case 185:
      return "24 24 34 37;";

    case 186:
      return "24 25 34 37;";

    case 187:
      return "25 25 34 37;";

    case 188:
      return "25 26 34 37;";

    case 189:
      return "26 26 34 37;";

    case 190:
      return "26 27 34 37;";

    case 191:
      return "27 27 34 37;";

    case 192:
      return "27 28 34 37;";

    case 193:
      return "28 28 34 37;";

    case 194:
      return "28 29 34 37;";

    case 195:
      return "29 29 34 37;";

    case 196:
      return "29 30 34 37;";

    case 197:
      return "30 30 34 37;";

    case 198:
      return "30 31 34 37;";

    case 199:
      return "31 31 34 37;";

    case 200:
      return "31 24 34 49;";

    case 201:
      return "24 24 46 49;";

    case 202:
      return "24 23 46 49;";

    case 203:
      return "23 23 46 49;";

    case 204:
      return "23 22 46 49;";

    case 205:
      return "22 22 46 49;";

    case 206:
      return "22 21 46 49;";

    case 207:
      return "21 21 46 49;";

    case 208:
      return "21 20 46 49;";

    case 209:
      return "20 20 46 49;";

    case 210:
      return "20 19 46 49;";

    case 211:
      return "19 19 46 49;";

    case 212:
      return "19 18 46 49;";

    case 213:
      return "18 18 46 49;";

    case 214:
      return "18 17 46 49;";

    case 215:
      return "17 17 46 49;";

    case 216:
      return "17 16 46 49;";

    case 217:
      return "16 16 46 49;";

    case 218:
      return "16 0 46 41;";

    case 219:
      return "0 0 38 41;";

    case 220:
      return "0 1 38 41;";

    case 221:
      return "1 1 38 41;";

    case 222:
      return "1 2 38 41;";

    case 223:
      return "2 2 38 41;";

    case 224:
      return "2 3 38 41;";

    case 225:
      return "3 3 38 41;";

    case 226:
      return "3 4 38 41;";

    case 227:
      return "4 4 38 41;";

    case 228:
      return "4 5 38 41;";

    case 229:
      return "5 5 38 41;";

    case 230:
      return "5 6 38 41;";

    case 231:
      return "6 6 38 41;";

    case 232:
      return "6 7 38 41;";

    case 233:
      return "7 7 38 41;";

    case 234:
      return "7 8 38 41;";

    case 235:
      return "8 8 38 41;";

    case 236:
      return "8 9 38 41;";

    case 237:
      return "9 9 38 41;";

    case 238:
      return "9 10 38 41;";

    case 239:
      return "10 10 38 41;";

    case 240:
      return "10 11 38 41;";

    case 241:
      return "11 11 38 41;";

    case 242:
      return "11 12 38 41;";

    case 243:
      return "12 12 38 41;";

    case 244:
      return "12 13 38 41;";

    case 245:
      return "13 13 38 41;";

    case 246:
      return "13 14 38 41;";

    case 247:
      return "14 14 38 41;";

    case 248:
      return "14 15 38 41;";

    case 249:
      return "15 15 38 41;";

    case 250:
      return "15 25 38 49;";

    case 251:
      return "25 25 46 49;";

    case 252:
      return "25 26 46 49;";

    case 253:
      return "26 26 46 49;";

    case 254:
      return "26 27 46 49;";

    case 255:
      return "27 27 46 49;";

    case 256:
      return "27 28 46 49;";

    case 257:
      return "28 28 46 49;";

    case 258:
      return "28 29 46 49;";

    case 259:
      return "29 29 46 49;";

    case 260:
      return "29 30 46 49;";

    case 261:
      return "30 30 46 49;";

    case 262:
      return "30 31 46 49;";

    case 263:
      return "31 31 46 49;";

    case 264:
      return "31 15 46 49;";

    case 265:
      return "15 15 46 49;";

    case 266:
      return "15 14 46 49;";

    case 267:
      return "14 14 46 49;";

    case 268:
      return "14 16 46 41;";

    case 269:
      return "16 16 38 41;";

    case 270:
      return "16 17 38 41;";

    case 271:
      return "17 17 38 41;";

    case 272:
      return "17 18 38 41;";

    case 273:
      return "18 18 38 41;";

    case 274:
      return "18 19 38 41;";

    case 275:
      return "19 19 38 41;";

    case 276:
      return "19 20 38 41;";

    case 277:
      return "20 20 38 41;";

    case 278:
      return "20 21 38 41;";

    case 279:
      return "21 21 38 41;";

    case 280:
      return "21 22 38 41;";

    case 281:
      return "22 22 38 41;";

    case 282:
      return "22 23 38 41;";

    case 283:
      return "23 23 38 41;";

    case 284:
      return "23 24 38 41;";

    case 285:
      return "24 24 38 41;";

    case 286:
      return "24 25 38 41;";

    case 287:
      return "25 25 38 41;";

    case 288:
      return "25 26 38 41;";

    case 289:
      return "26 26 38 41;";

    case 290:
      return "26 27 38 41;";

    case 291:
      return "27 27 38 41;";

    case 292:
      return "27 28 38 41;";

    case 293:
      return "28 28 38 41;";

    case 294:
      return "28 29 38 41;";

    case 295:
      return "29 29 38 41;";

    case 296:
      return "29 30 38 41;";

    case 297:
      return "30 30 38 41;";

    case 298:
      return "30 31 38 41;";

    case 299:
      return "31 31 38 41;";

    case 300:
      return "31 0 38 45;";

    case 301:
      return "0 0 42 45;";

    case 302:
      return "0 1 42 45;";

    case 303:
      return "1 1 42 45;";

    case 304:
      return "1 2 42 45;";

    case 305:
      return "2 2 42 45;";

    case 306:
      return "2 3 42 45;";

    case 307:
      return "3 3 42 45;";

    case 308:
      return "3 4 42 45;";

    case 309:
      return "4 4 42 45;";

    case 310:
      return "4 5 42 45;";

    case 311:
      return "5 5 42 45;";

    case 312:
      return "5 6 42 45;";

    case 313:
      return "6 6 42 45;";

    case 314:
      return "6 7 42 45;";

    case 315:
      return "7 7 42 45;";

    case 316:
      return "7 8 42 45;";

    case 317:
      return "8 8 42 45;";

    case 318:
      return "8 9 42 45;";

    case 319:
      return "11 9 26 45;";

    case 320:
      return "11 11 26 29;";

    case 321:
      return "10 11 26 29;";

    case 322:
      return "10 10 26 29;";

    case 323:
      return "9 10 26 29;";

    case 324:
      return "9 9 26 29;";

    case 325:
      return "8 9 26 29;";

    case 326:
      return "8 8 26 29;";

    case 327:
      return "7 8 26 29;";

    case 328:
      return "7 7 26 29;";

    case 329:
      return "6 7 26 29;";

    case 330:
      return "6 6 26 29;";

    case 331:
      return "5 6 26 29;";

    case 332:
      return "5 5 26 29;";

    case 333:
      return "4 5 26 29;";

    case 334:
      return "4 4 26 29;";

    case 335:
      return "3 4 26 29;";

    case 336:
      return "3 3 26 29;";

    case 337:
      return "2 3 26 29;";

    default:
      return "";
    }
}
