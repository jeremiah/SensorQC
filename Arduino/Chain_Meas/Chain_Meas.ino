//Pin connected to clock pin (SH_CP) of 74HC595
const int clockPin = 11;
////Pin connected to Data in (DS) of 74HC595
const int Sync_0_r = 49;
const int Sync_1_r = 45;
const int Sync_2_r = 41;
const int Sync_3_r = 37;
const int Sync_4_r = 33;
const int Sync_5_r = 29;

const int Sync_0_l = 46;
const int Sync_1_l = 42;
const int Sync_2_l = 38;
const int Sync_3_l = 34;
const int Sync_4_l = 30;
const int Sync_5_l = 26;

const int dataPin_l = 50;
const int dataPin_r = 53;

int Chain_ID_l;
int Chain_ID_r;
int Sync_pin_l=-1;
int Sync_pin_r=-1;

void setup() {
  //set pins to output because they are addressed in the main loop
  pinMode(Sync_0_r, OUTPUT);
  pinMode(Sync_1_r, OUTPUT);
  pinMode(Sync_2_r, OUTPUT);
  pinMode(Sync_3_r, OUTPUT);
  pinMode(Sync_4_r, OUTPUT);
  pinMode(Sync_5_r, OUTPUT);

  pinMode(Sync_0_l, OUTPUT);
  pinMode(Sync_1_l, OUTPUT);
  pinMode(Sync_2_l, OUTPUT);
  pinMode(Sync_3_l, OUTPUT);
  pinMode(Sync_4_l, OUTPUT);
  pinMode(Sync_5_l, OUTPUT);

  pinMode(dataPin_r, OUTPUT);  
  pinMode(dataPin_l, OUTPUT);
  pinMode(clockPin, OUTPUT);
  
  digitalWrite(Sync_0_l, HIGH);
  digitalWrite(Sync_1_l, HIGH);
  digitalWrite(Sync_2_l, HIGH);
  digitalWrite(Sync_3_l, HIGH);
  digitalWrite(Sync_4_l, HIGH);
  digitalWrite(Sync_5_l, HIGH);

  digitalWrite(Sync_0_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_0_l, HIGH);
  
  digitalWrite(Sync_1_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_1_l, HIGH);

  digitalWrite(Sync_2_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_2_l, HIGH);
  
  digitalWrite(Sync_3_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_3_l, HIGH);

  digitalWrite(Sync_4_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_4_l, HIGH);

  digitalWrite(Sync_5_l, LOW);
  shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_5_l, HIGH);

  digitalWrite(Sync_0_r, HIGH);
  digitalWrite(Sync_1_r, HIGH);
  digitalWrite(Sync_2_r, HIGH);
  digitalWrite(Sync_3_r, HIGH);
  digitalWrite(Sync_4_r, HIGH);
  digitalWrite(Sync_5_r, HIGH);
  
  digitalWrite(Sync_0_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_0_r, HIGH);
  
  digitalWrite(Sync_1_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_1_r, HIGH);

  digitalWrite(Sync_2_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_2_r, HIGH);
  
  digitalWrite(Sync_3_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_3_r, HIGH);

  digitalWrite(Sync_4_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_4_r, HIGH);

  digitalWrite(Sync_5_r, LOW);
  shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
  digitalWrite(Sync_5_r, HIGH);
  
  Serial.begin(9600);
  Serial.println("reset");
}
void loop() {
  if (Serial.available() > 0) {
      digitalWrite(Sync_0_l, HIGH);
      digitalWrite(Sync_1_l, HIGH);
      digitalWrite(Sync_2_l, HIGH);
      digitalWrite(Sync_3_l, HIGH);
      digitalWrite(Sync_4_l, HIGH);
      digitalWrite(Sync_5_l, HIGH);
    
      digitalWrite(Sync_0_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_0_l, HIGH);
      
      digitalWrite(Sync_1_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_1_l, HIGH);
    
      digitalWrite(Sync_2_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_2_l, HIGH);
      
      digitalWrite(Sync_3_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_3_l, HIGH);
    
      digitalWrite(Sync_4_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_4_l, HIGH);
    
      digitalWrite(Sync_5_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_5_l, HIGH);
    
      digitalWrite(Sync_0_r, HIGH);
      digitalWrite(Sync_1_r, HIGH);
      digitalWrite(Sync_2_r, HIGH);
      digitalWrite(Sync_3_r, HIGH);
      digitalWrite(Sync_4_r, HIGH);
      digitalWrite(Sync_5_r, HIGH);
      
      digitalWrite(Sync_0_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_0_r, HIGH);
      
      digitalWrite(Sync_1_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_1_r, HIGH);
    
      digitalWrite(Sync_2_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_2_r, HIGH);
      
      digitalWrite(Sync_3_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_3_r, HIGH);
    
      digitalWrite(Sync_4_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_4_r, HIGH);
    
      digitalWrite(Sync_5_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, 255);
      digitalWrite(Sync_5_r, HIGH);
    Chain_ID_l = Serial.parseInt();
    Chain_ID_r = Serial.parseInt();
    Sync_pin_l = Serial.parseInt();
    Sync_pin_r = Serial.parseInt();

    if (Serial.read() == ';'){
      digitalWrite(Sync_pin_l, LOW);
      shiftOut(dataPin_l, clockPin, MSBFIRST, Chain_ID_l);
      digitalWrite(Sync_pin_l, HIGH);
      digitalWrite(Sync_pin_l, HIGH);
      digitalWrite(dataPin_l, LOW);
      
      digitalWrite(Sync_pin_r, LOW);
      shiftOut(dataPin_r, clockPin, MSBFIRST, Chain_ID_r);
      digitalWrite(Sync_pin_r, HIGH);
      digitalWrite(Sync_pin_r, HIGH);
      digitalWrite(dataPin_r, LOW);
    }
  }
}
