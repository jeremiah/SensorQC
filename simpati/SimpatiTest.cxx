#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <string.h>
#include <thread>
#include <chrono>

#include "simpati.h"

int main(int /*argc*/, char **/*argv*/)
{
  simpati *mychamber = 0;
  try{
    mychamber = new simpati();
  }catch(...){
    std::cerr << "Unknown exception while executing" << std::endl;
    return -1;
  }
  std::map<int, std::string> names = mychamber->getNames();
  mychamber->setEnabled(true);
  for(int i=0;i<5;i++){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    std::cout << "Name     \tSet\tMeas."<<std::endl;
    for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
      std::pair<double, double> vals = mychamber->getVals()[it->first];
      std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
    }
    std::cout << std::endl;
  }
  std::cout << "changing setting" << std::endl;
  mychamber->setCage(false, true);
  mychamber->setTwarm(35.);
  mychamber->setTcold(5.);
  std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  std::cout << "Name     \tSet\tMeas."<<std::endl;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = mychamber->getVals()[it->first];
    std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
  }
  std::cout << std::endl;

  std::cout << "changing setting back" << std::endl;
  mychamber->setCage(true, true);
  mychamber->setTwarm(30.);
  mychamber->setTcold(15.);
  mychamber->setEnabled(false);
  std::this_thread::sleep_for (std::chrono::milliseconds(1000));
  std::cout << "Name     \tSet\tMeas."<<std::endl;
  for(std::map<int, std::string>::iterator it = names.begin(); it!=names.end();it++){
    std::pair<double, double> vals = mychamber->getVals()[it->first];
    std::cout << it->second<<"\t"<<vals.first<<"\t"<<vals.second<<std::endl;
  }
  std::cout << std::endl;

  delete mychamber;

  return 0;
}
