TEMPLATE = app

CONFIG += qt
QT += network
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}
unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

INCLUDEPATH += . ../PixGPIB ../PixRS232 ../libSuess ../IVandXYscan ../include

FORMS += IVscan.ui ProberControl.ui TempMon.ui

SOURCES += IVandXYscan.cxx IVscan.cxx ProberControl.cxx \
	   TempMon.cxx QRootApplication.cxx

HEADERS += IVandXYscan.h IVscan.h ProberControl.h \
	   TempMon.h QRootApplication.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        INCLUDEPATH += $${system(root-config --incdir)}
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB
        QMAKE_RPATHDIR += . ../PixRS232 ../PixGPIB ../libSuess
        LIBS += -L ../libSuess -lSuess -L../PixRS232 -lPixRS232 -L../PixGPIB -lPixGPIB
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../PixGPIB /LIBPATH:../bin /LIBPATH:$(ROOTSYS)/lib
    LIBS += Suess.lib PixRS232.lib PixGPIB.lib GPIB-32.obj
    LIBS += libCore.lib
    LIBS += libCint.lib 
    LIBS += libRIO.lib 
    LIBS += libNet.lib 
    LIBS += libHist.lib 
    LIBS += libGraf.lib 
    LIBS += libGraf3d.lib 
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
}

