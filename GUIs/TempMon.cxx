#include <TSystem.h>
#include <TF1.h>
#include <TMath.h>

#include <TempMon.h>
#include <QFileDialog>
#include <QDateTime>
#include <QTcpSocket>
#include <QHostAddress>
#include <QCoreApplication>

#include "PixGPIBDevice.h"
#include "PixRs232Device.h"
#include "VA18B.h"

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

TempMon::TempMon(QWidget * parent, Qt::WindowFlags flags)
  : QWidget(parent, flags) {
  setupUi(this);

  m_meterReadings[0] = meterReading_0; m_meterReadings[1] = meterReading_1; m_meterReadings[2] = meterReading_2; m_meterReadings[3] = meterReading_3; m_meterReadings[4] = meterReading_4; m_meterReadings[5] = meterReading_5; m_meterReadings[6] = meterReading_6;
  m_checkBox[0] = checkBox_0; m_checkBox[1] = checkBox_1; m_checkBox[2] = checkBox_2; m_checkBox[3] = checkBox_3; m_checkBox[4] = checkBox_4; m_checkBox[5] = checkBox_5; m_checkBox[6] = checkBox_6;
  m_gpibLabels[0]=gpibLabel_0; m_gpibLabels[1]=gpibLabel_1; m_gpibLabels[2]=gpibLabel_2; m_gpibLabels[3]=gpibLabel_3; m_gpibLabels[4]=gpibLabel_4; m_gpibLabels[5]=gpibLabel_5; m_gpibLabels[6]=gpibLabel_6;
  m_gpibPADs[0]=gpibPAD_0; m_gpibPADs[1]=gpibPAD_1; m_gpibPADs[2]=gpibPAD_2; m_gpibPADs[3]=gpibPAD_3; m_gpibPADs[4]=gpibPAD_4; m_gpibPADs[5]=gpibPAD_5; m_gpibPADs[6]=gpibPAD_6;
  m_calibBox[0]=calibBox_0; m_calibBox[1]=calibBox_1; m_calibBox[2]=calibBox_2; m_calibBox[3]=calibBox_3; m_calibBox[4]=calibBox_4; m_calibBox[5]=calibBox_5; m_calibBox[6]=calibBox_6;
  m_gpibComboBox[0]=gpib_ComboBox_0; m_gpibComboBox[1]=gpib_ComboBox_1; m_gpibComboBox[2]=gpib_ComboBox_2; m_gpibComboBox[3]=gpib_ComboBox_3; m_gpibComboBox[4]=gpib_ComboBox_4; m_gpibComboBox[5]=gpib_ComboBox_5; m_gpibComboBox[6]=gpib_ComboBox_6;
  m_ipAdds[0] = ipAdd_0; m_ipAdds[1] = ipAdd_1; m_ipAdds[2] = ipAdd_2; m_ipAdds[3] = ipAdd_3; m_ipAdds[4] = ipAdd_4; m_ipAdds[5] = ipAdd_5; m_ipAdds[6] = ipAdd_6;
  m_unitLabels[0] = setVLabel_0; m_unitLabels[1] = setVLabel_1; m_unitLabels[2] = setVLabel_2; m_unitLabels[3] = setVLabel_3; m_unitLabels[4] = setVLabel_4; m_unitLabels[5] = setVLabel_5; m_unitLabels[6] = setVLabel_6;
  m_chanLabels[0] = chanLabel_0, m_chanLabels[1] = chanLabel_1, m_chanLabels[2] = chanLabel_2, m_chanLabels[3] = chanLabel_3; m_chanLabels[4] = chanLabel_4; m_chanLabels[5] = chanLabel_5; m_chanLabels[6] = chanLabel_6;
  m_devChans[0] = devChan_0, m_devChans[1] = devChan_1, m_devChans[2] = devChan_2, m_devChans[3] = devChan_3; m_devChans[4] = devChan_4; m_devChans[5] = devChan_5; m_devChans[6] = devChan_6;

  for(uint i=0;i<nitems;i++){
    m_dev[i] = NULL;
    m_com[i] = NULL;
    m_va[i]  = NULL;
    m_gpibComboBox[i]->addItem(QString("RS232"));
    m_gpibComboBox[i]->addItem(QString("DataLogger"));
    m_gpibComboBox[i]->addItem(QString("VA18B"));
    typeChg(m_gpibComboBox[i]->currentText(), i);
    connect(m_checkBox[i], SIGNAL(toggled(bool)), this, SLOT(devSelected(bool)));
  }

  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(nDUTbox, SIGNAL(valueChanged(int)), this, SLOT(setNDUT(int)));
  connect(m_gpibComboBox[0], SIGNAL(activated(QString)), this, SLOT(typeChg0(QString)));
  connect(m_gpibComboBox[1], SIGNAL(activated(QString)), this, SLOT(typeChg1(QString)));
  connect(m_gpibComboBox[2], SIGNAL(activated(QString)), this, SLOT(typeChg2(QString)));
  connect(m_gpibComboBox[3], SIGNAL(activated(QString)), this, SLOT(typeChg3(QString)));
  connect(m_gpibComboBox[4], SIGNAL(activated(QString)), this, SLOT(typeChg4(QString)));
  connect(m_gpibComboBox[5], SIGNAL(activated(QString)), this, SLOT(typeChg5(QString)));
  connect(m_gpibComboBox[6], SIGNAL(activated(QString)), this, SLOT(typeChg6(QString)));

  setNDUT(nDUTbox->value());

}

TempMon::~TempMon(){
  disconnDev();
}
void TempMon::connDev(){
  QList<QString> myTypes;
  m_readLogger.clear();

  for(uint i=0;i<nitems;i++){

    if(m_checkBox[i]->isChecked()){
      if(m_gpibComboBox[i]->currentText()=="GPIB"){
	m_dev[i] = NULL;
	// check if same device (with other channel) is used already -> of so, just copy pointer
	for(uint k=0;k<i;k++) {
	  if(m_dev[k] != NULL && m_gpibPADs[i]->value()==m_gpibPADs[k]->value() && 
	     m_gpibComboBox[k]->currentText()=="GPIB") {
	    m_dev[i] = m_dev[k];
	    break;
	  }
	}
	if(m_dev[i] == NULL)
	  m_dev[i] = new PixGPIBDevice(0, m_gpibPADs[i]->value(), 0, false); 
	if(m_dev[i]->getStatus()==PixGPIBDevice::PGD_ERROR){
	  delete m_dev[i]; m_dev[i] = NULL;
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	  std::cerr << "Error contacting GPIB device on " << m_gpibPADs[i]->value() << std::endl;
	} else{
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  int nch = m_dev[i]->getDeviceNumberChannels();
	  if(nch>1){
	    m_devChans[i]->setMaximum(nch-1);
	  } else {
	    m_devChans[i]->hide();
	    m_chanLabels[i]->hide();
	  }
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	  if(m_checkBox[i]->text()=="Rel. Humidity") checkBox_DP->setEnabled(false);
	}
      } else if(m_gpibComboBox[i]->currentText()=="RS232"){
	// to do 
      } else if(m_gpibComboBox[i]->currentText()=="VA18B"){
	m_va[i] = new VA18B(m_gpibPADs[i]->value());
	QCoreApplication::processEvents();
	// wait for device to get ready
	int ium=0;
	while(m_va[i]->getMeasUnit()=="unknown" && ium<10){
	  ium++;
	  std::this_thread::sleep_for (std::chrono::milliseconds(500));  
	}
	if(ium>10){
	  delete m_va[i]; m_va[i] = NULL;
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	  std::cerr << "Error contacting VA18B device on " << m_gpibPADs[i]->value() << std::endl;	  
	} else {
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  m_devChans[i]->hide();
	  m_chanLabels[i]->hide();
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	}
      } else {
	QStringList sl = m_ipAdds[i]->text().split(":");
	QString ipadd = sl.at(0);// = "192.168.2.153";
	int port = sl.at(1).toInt();// = 7700;
	if(m_readLogger.find(ipadd)==m_readLogger.end() || !m_readLogger[ipadd]) {
	  //std::cout << "connecting IP " << std::string(ipadd.toLatin1().data()) << " on port " << port << std::endl;
	  m_readLogger[ipadd] = false;
	  m_thrDL.push_back(std::thread(&TempMon::readLogger, this, ipadd, port));
	}
	QCoreApplication::processEvents();
	std::this_thread::sleep_for (std::chrono::milliseconds(1000));
	if(m_readLogger[ipadd]){
	  m_gpibComboBox[i]->setEnabled(false);
	  m_checkBox[i]->setEnabled(false);
	  m_gpibPADs[i]->setEnabled(false);
	  m_devChans[i]->hide();
	  m_chanLabels[i]->hide();
	  myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
	  //if(m_checkBox[i]->text()=="Rel. Humidity") checkBox_DP->setEnabled(false);
	} else {
	  m_gpibLabels[i]->setText("Error");
	  m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	}
      }
    } else { // hide unused elements
      m_checkBox[i]->hide();
      m_meterReadings[i]->hide();
      m_calibBox[i]->hide();
      m_gpibLabels[i]->hide();
      m_unitLabels[i]->hide();
      m_gpibPADs[i]->hide();
      m_gpibComboBox[i]->hide();
      m_ipAdds[i]->hide();
      m_devChans[i]->hide();
      m_chanLabels[i]->hide();
    }
    QCoreApplication::processEvents();
  }

  checkBox_DP->setEnabled(false);
  if(checkBox_DP->isChecked()) myTypes.push_back(checkBox_DP->text()+"("+setVLabel_DP->text()+")");

//   readMeters();
  m_thrRuns = true;
  m_thread = std::thread(&TempMon::readMeters, this);
  emit newTempDev(myTypes);

  gpibConnectButton->setText("Disconnect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void TempMon::disconnDev(){
  m_thrRuns = false;
  if(m_thread.joinable()) m_thread.join();
  QList<QString> myTypes;
  for(QMap<QString,bool>::iterator it = m_readLogger.begin(); it!=m_readLogger.end(); it++)
    it.value() = false;
  for(std::vector<std::thread>::iterator it = m_thrDL.begin(); it!=m_thrDL.end(); it++)
    if(it->joinable()) it->join();
  m_readLogger.clear();
  
  for(uint i=0;i<nitems;i++){
    if(m_checkBox[i]->isChecked()){
      m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
      typeChg(m_gpibComboBox[i]->currentText(), i);
      m_gpibComboBox[i]->setEnabled(true);
      // if this device was already deleted elsewhere, don't delete again
      for(uint k=0;k<i;k++) {
	if(m_dev[k] == NULL && m_gpibPADs[i]->value()==m_gpibPADs[k]->value() && 
	   m_gpibComboBox[k]->currentText()=="GPIB") {
	  m_dev[i] = NULL;
	  break;
	}
      }
      m_checkBox[i]->setEnabled(true);
      delete m_dev[i]; m_dev[i] = NULL;
      delete m_com[i]; m_com[i] = NULL;
      delete m_va[i]; m_va[i] = NULL;
      myTypes.push_back(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")");
    } else { // show prev. unused elements
      m_checkBox[i]->show();
      m_meterReadings[i]->show();
      m_calibBox[i]->show();
      m_gpibLabels[i]->show();
      m_unitLabels[i]->show();
      m_gpibComboBox[i]->show();
      typeChg(m_gpibComboBox[i]->currentText(), i);
    }
    m_devChans[i]->show();
    m_chanLabels[i]->show();
    QCoreApplication::processEvents();
  }
  for(uint i=0;i<nitems;i++)
    emit m_checkBox[i]->toggled(m_checkBox[i]->isChecked());
  
  checkBox_DP->setEnabled(true);
  if(checkBox_DP->isChecked()) myTypes.push_back(checkBox_DP->text()+"("+setVLabel_DP->text()+")");

  emit obsTempDev(myTypes);
  gpibConnectButton->setText("Connect Selected");
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDev()));
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDev()));
}
void TempMon::readMeters(){

  while(m_thrRuns){

    double rawMeas, calMeas=0., Tamb=20.;
    QMap<QString, double> Tmap;
    
    for(uint i=0;i<nitems;i++){
      
      if(m_checkBox[i]->isChecked() && !m_checkBox[i]->isEnabled()){
	rawMeas = -1.;
	if(m_gpibComboBox[i]->currentText()=="GPIB"){
	  if(m_dev[i]!=NULL){
	    if(m_checkBox[i]->text()=="Rel. Humidity"){
	      m_dev[i]->measureVoltages(0., true, m_devChans[i]->value());
	      rawMeas = m_dev[i]->getVoltage(m_devChans[i]->value());
	    } else {
	      m_dev[i]->measureResistances(0., true, m_devChans[i]->value());
	      rawMeas = m_dev[i]->getResistance(m_devChans[i]->value());
	    }
	  }
	  if(m_dev[i]->getStatus()==PixGPIBDevice::PGD_ERROR){
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	    std::cerr << "Error reading from GPIB device on " << m_gpibPADs[i]->value() << std::endl;
	  } else
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	} else if(m_gpibComboBox[i]->currentText()=="RS232"){
	  // to do
	} else if(m_gpibComboBox[i]->currentText()=="VA18B"){
	  std::string unit = m_va[i]->getMeasUnit();
	  if((m_checkBox[i]->text()=="Rel. Humidity" && unit=="V") || 
	     unit=="Ohm" || unit=="°C")
	    rawMeas = m_va[i]->getMeasVal();
	  else 
	    rawMeas = -1.;
	  if(unit=="unknown"){
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: red}");
	    std::cerr << "Error reading from VA18B:device on " << m_gpibPADs[i]->value() << ": no recent data available!"<< std::endl;	  
	  } else
	    m_gpibLabels[i]->setStyleSheet("QLabel {background-color: #efebe7}");
	} else if(m_gpibComboBox[i]->currentText()=="DataLogger"){
	  if(m_loggerVals.find(m_checkBox[i]->text())!=m_loggerVals.end()) {
	    calMeas = m_loggerVals[m_checkBox[i]->text()];
	    rawMeas = 0.;
	  }
	}
	if(rawMeas>0.){
	  if(m_calibBox[i]->currentText()=="direct")
	    calMeas = rawMeas;
	  else if(m_calibBox[i]->currentText()=="PT1000")
	    calMeas = convPT1000(rawMeas);
	  else if(m_calibBox[i]->currentText()=="PT100")
	    calMeas = convPT100(rawMeas);
	  else if(m_calibBox[i]->currentText()=="NTC")
	    calMeas = convNTC(rawMeas,0);
	  else if(m_calibBox[i]->currentText()=="Mod-NTC")
	    calMeas = convNTC(rawMeas,1);
	  else if(m_calibBox[i]->currentText()=="HIH4000")
	    calMeas = convHIH4000(rawMeas, 5.0, Tamb);
	  else{
	    calMeas = 0.;
	    std::cerr << "Unknown calibration method: " << m_calibBox[i]->currentText().toLatin1().data() << std::endl;
	  }
	}
	if(rawMeas>=0.){
	  if(m_checkBox[i]->text()=="Ambient T") Tamb = calMeas;
	  if(m_checkBox[i]->text()=="Rel. Humidity" && checkBox_DP->isChecked()){ // calculate dew point
	    double dewpt = (241.2*TMath::Log(calMeas/100.)+4222.03716*Tamb/(241.2+Tamb)) / 
	      (17.5043 - TMath::Log(calMeas/100.)-17.5043*Tamb/(241.2+Tamb));
	    meterReading_DP->display(convDispl(dewpt));
	    Tmap.insert(checkBox_DP->text()+"("+setVLabel_DP->text()+")", dewpt);
	  }
	  m_meterReadings[i]->display(convDispl(calMeas));
	  Tmap.insert(m_checkBox[i]->text()+"("+m_unitLabels[i]->text()+")", calMeas);
	}
      }
    }
    //emit newTempVal(Tmap);
    setNewRdgEvent *nre = new setNewRdgEvent(Tmap);
    QCoreApplication::postEvent(this, nre);
    std::this_thread::sleep_for (std::chrono::milliseconds(500));  
  }

}
double TempMon::convNTC(double res, int type){
  double cal[4]={0.0020091, 0.000296397, 1.65669e-06, -175.422};
  double cal2[4]={0.0002911, 0.00268, 0.003354,  273.15};
  switch(type){
  default:
  case 0: // "blue drop" NTC
    return 1/(cal[0]+cal[1]*TMath::Log(res)+cal[2]*TMath::Power(TMath::Log(res),3))+cal[3];;
  case 1: // demonstrator module NTC
    return 1/(cal2[0]* TMath::Log(res)-cal2[1]+cal2[2])-cal2[3];
  }
}
double TempMon::convPT1000(double res){
  static TF1 fpos("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x)", 0, 100);
  static TF1 fneg("pt1000","1000.*(1+3.9083e-3*x-5.775e-7*x*x-4.183e-12*(x-100)*x*x*x)", -100, 0);
  return (res<1000.)?fneg.GetX(res):fpos.GetX(res);
}
double TempMon::convPT100(double res){
  // TEMPORARY: needs correct calibration 
  static TF1 fpos("pt100","100.*(1+3.9083e-3*x-5.775e-7*x*x)", 0, 100);
  static TF1 fneg("pt100","100.*(1+3.9083e-3*x-5.775e-7*x*x-4.183e-12*(x-100)*x*x*x)", -100, 0);
  return (res<100.)?fneg.GetX(res):fpos.GetX(res);
}
double TempMon::convHIH4000(double measV, double nomV, double ambT){
  return (measV/nomV-0.16)/0.0062/(1.0546-0.00216*ambT);;
}
QString TempMon::convDispl(double value){
  char cstrg[50];
  sprintf(cstrg, "%.1lf", value);
  return QString(cstrg);
  //return QString::number((int)value) + "." + QString("%1").arg(qRound((value-(double)(int)value) * 10), 1, 10, QChar('0'));
}
void TempMon::typeChg(QString type, uint item){
  if(item>=nitems) return;
  if(type=="GPIB"){
    m_gpibLabels[item]->setText("PAD");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else if(type=="RS232"){
    m_gpibLabels[item]->setText("COM");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else if(type=="VA18B"){
    m_gpibLabels[item]->setText("USB");
    m_gpibPADs[item]->show();
    m_calibBox[item]->show();
    m_ipAdds[item]->hide();
  } else {
    m_gpibLabels[item]->setText("   ");
    m_gpibPADs[item]->hide();
    m_calibBox[item]->hide();
    m_ipAdds[item]->show();
  }
}
void TempMon::readLogger(QString ipadd, int port){
  uint len_in, len_out;
  //char bufc[1024], bufr[1024];

  m_loggerVals.clear();

  QTcpSocket *sock = new QTcpSocket;
  const int myTimeout = 1000;

  sock->connectToHost(ipadd, port);
  if (!sock->waitForConnected(myTimeout)){
    std::cerr << "Timeout connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return;
  }
  if (sock->state() != QAbstractSocket::ConnectedState){
    std::cerr << "Error connecting to " << ipadd.toStdString() << std::endl;
    delete sock;
    return;
  }

  QByteArray block;
  block[0] = 127;
  len_in = 1;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	sock->close();
	delete sock;
	return;
      }
    }
    int idx = resp.indexOf('\n');
    if (idx < 1 || resp.left(2)!="<<"){
      std::cerr << "Error: lost connection to " << ipadd.toStdString() << " - " << resp.toStdString()<< std::endl;
      sock->close();
      delete sock;
      return;
    }
  }

  m_readLogger[ipadd] = true;

  block.clear();
  block[0]='/';
  block[1]='E';
  block[2]='/';
  block[3]='M';
  block[4]='/';
  block[5]='R';
  block[6]='\r';
  len_in = 7;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	sock->close();
	delete sock;
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	return;
      }
    }
  }

  while(m_readLogger[ipadd]){
    std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    if (sock->state() == QAbstractSocket::ConnectedState){
      QByteArray resp;
      int tries = 0;
      while((!resp.contains('\n')) && (tries++ < 600)) {
	sock->waitForReadyRead(100);
	resp += sock->readAll();
	if (sock->state() != QAbstractSocket::ConnectedState){
	  sock->close();
	  delete sock;
	  std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	  return;
	}
      }
      int idx = resp.indexOf('\n');
      if (idx < 1){
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	sock->close();
	delete sock;
	return;
      }
      QString str(resp);//bufr);
      QStringList list = str.split('\n', QString::SkipEmptyParts);
      for(int i=0; i<list.size();i++){
	QString listel = list.at(i);
	//std::cout << "Found item " << listel.toStdString() << std::endl;
	if(listel.left(11)=="Amb Temp PT"){
	  listel.remove(0,12);
	  if(m_loggerVals.find("Ambient T")==m_loggerVals.end())
	    m_loggerVals.insert("Ambient T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Ambient T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(17)=="Chuck Temperature"){
	  listel.remove(0,18);
	  if(m_loggerVals.find("Chuck T")==m_loggerVals.end())
	    m_loggerVals.insert("Chuck T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Chuck T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(20)=="VacChuck Temperature"){
	  listel.remove(0,21);
	  if(m_loggerVals.find("Chuck T")==m_loggerVals.end())
	    m_loggerVals.insert("Chuck T", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Chuck T"] = listel.split(" ").at(0).toDouble();
	}
	if(listel.left(16)=="Ambient Humidity" || listel.left(8)=="Humidity"){
	  if(listel.left(16)=="Ambient Humidity") listel.remove(0,17);
	  else listel.remove(0,9);
	  if(m_loggerVals.find("Rel. Humidity")==m_loggerVals.end())
	    m_loggerVals.insert("Rel. Humidity", listel.split(" ").at(0).toDouble());
	  else
	    m_loggerVals["Rel. Humidity"] = listel.split(" ").at(0).toDouble();
	}
      }
      //std::cout << std::endl;
    }
  }

  block.clear();
  block[0]='/';
  block[1]='e';
  block[2]='/';
  block[3]='m';
  block[4]='/';
  block[5]='r';
  block[6]='\r';
  len_in = 7;

  len_out = sock->write(block);
  sock->waitForBytesWritten(myTimeout);
  if(len_out==len_in){
    std::this_thread::sleep_for (std::chrono::milliseconds(50));
    QByteArray resp;
    int tries = 0;
    while((!resp.contains('\n')) && (tries++ < 600)) {
      sock->waitForReadyRead(100);
      resp += sock->readAll();
      if (sock->state() != QAbstractSocket::ConnectedState){
	sock->close();
	std::cerr << "Error: lost connection to " << ipadd.toStdString() << std::endl;
	return;
      }
    }
  }

  m_loggerVals.clear();
  sock->close();
  delete sock;

}
void TempMon::devSelected(bool isSel){
  int icb = -1;
  for(int i=0; i<nitems;i++){
    if(m_checkBox[i]==QObject::sender()){
      icb = i;
      break;
    }
  }
  if(icb>=0 && icb<nitems){
    m_gpibLabels[icb]->setEnabled(isSel);
    m_gpibPADs[icb]->setEnabled(isSel);
    m_calibBox[icb]->setEnabled(isSel);
    m_gpibComboBox[icb]->setEnabled(isSel);
    m_ipAdds[icb]->setEnabled(isSel);
    m_unitLabels[icb]->setEnabled(isSel);
    m_chanLabels[icb]->setEnabled(isSel);
    m_devChans[icb]->setEnabled(isSel);
    m_meterReadings[icb]->setEnabled(isSel);
  }
}
void TempMon::customEvent( QEvent * event )
{
  if(event==0){
    std::cerr << "SuppControlBase::customEvent called with a NULL-pointer";
    return;
  }
  
  if(event->type()==2011){
    QMap<QString, double> valmap = dynamic_cast<setNewRdgEvent*>(event)->getVals();
    emit newTempVal(valmap);
  }
}
void TempMon::setNDUT(int ndut){
  for(uint i=0;i<nitems-1;i++){
    int idut = i-2;
    if(idut<ndut){
      m_checkBox[i]->show();
      m_meterReadings[i]->show();
      m_gpibPADs[i]->show();
      m_calibBox[i]->show();
      m_gpibLabels[i]->show();
      m_unitLabels[i]->show();
      m_gpibComboBox[i]->show();
      m_devChans[i]->show();
      m_chanLabels[i]->show();
      typeChg(m_gpibComboBox[i]->currentText(), i);
    } else{
      m_checkBox[i]->hide();
      m_meterReadings[i]->hide();
      m_gpibPADs[i]->hide();
      m_calibBox[i]->hide();
      m_gpibLabels[i]->hide();
      m_unitLabels[i]->hide();
      m_gpibComboBox[i]->hide();
      m_devChans[i]->hide();
      m_chanLabels[i]->hide();
    }
  }
}
