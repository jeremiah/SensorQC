#ifdef WIN32
#include <Windows4Root.h>
#endif
#include "QRootApplication.h"
#include "IVscan.h"
#include "PixGPIBDevice.h"
#include "PixRs232Device.h"

#include <iostream>
#include <sstream>
#include <exception>
#include <string>
#include <ctime>
#include <chrono>
#include <fstream>

#include <QString>
#include <QApplication>
#include <QFileDialog>

#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TFile.h>
#include <TROOT.h>

IVscan::IVscan(QWidget * parent, Qt::WindowFlags flags) : QWidget(parent, flags) {
  setupUi(this);
  flagAbortScan = false;
  m_can = NULL;
  m_canAllXYscan = NULL;
  legAllXYscan = NULL;
  maxNumInstr = 6;
  numInstrEnabled = 0;

  stepX=-1;
  stepY=-1;
  posX=-1;
  posY=-1;
  iScan=0;
  NScan=1;

  m_ChuckT=-9999;
  m_AmbientT=-9999;
  m_RH=-9999;

  // fill vectors of instrument parameters
  sensorPart_.push_back(sensorPart_0); sensorPart_.push_back(sensorPart_1); sensorPart_.push_back(sensorPart_2); sensorPart_.push_back(sensorPart_3); sensorPart_.push_back(sensorPart_4); sensorPart_.push_back(sensorPart_5);
  checkBoxHV_.push_back(checkBoxHV_0); checkBoxHV_.push_back(checkBoxHV_1); checkBoxHV_.push_back(checkBoxHV_2); checkBoxHV_.push_back(checkBoxHV_3); checkBoxHV_.push_back(checkBoxHV_4); checkBoxHV_.push_back(checkBoxHV_5);
  checkBoxImeas_.push_back(checkBoxImeas_0); checkBoxImeas_.push_back(checkBoxImeas_1); checkBoxImeas_.push_back(checkBoxImeas_2); checkBoxImeas_.push_back(checkBoxImeas_3); checkBoxImeas_.push_back(checkBoxImeas_4); checkBoxImeas_.push_back(checkBoxCmeas_5);
  gpibPAD_.push_back(gpibPAD_0); gpibPAD_.push_back(gpibPAD_1); gpibPAD_.push_back(gpibPAD_2); gpibPAD_.push_back(gpibPAD_3); gpibPAD_.push_back(gpibPAD_4); gpibPAD_.push_back(gpibPAD_5);
  instrumentChan_.push_back(instrumentChan_0); instrumentChan_.push_back(instrumentChan_1); instrumentChan_.push_back(instrumentChan_2); instrumentChan_.push_back(instrumentChan_3); instrumentChan_.push_back(instrumentChan_4); instrumentChan_.push_back(instrumentChan_5);
  setVolt_.push_back(setVolt_0); setVolt_.push_back(setVolt_1); setVolt_.push_back(setVolt_2); setVolt_.push_back(setVolt_3); setVolt_.push_back(setVolt_4); setVolt_.push_back(setVolt_5);
  measCurr_.push_back(measCurr_0); measCurr_.push_back(measCurr_1); measCurr_.push_back(measCurr_2); measCurr_.push_back(measCurr_3); measCurr_.push_back(measCurr_4); measCurr_.push_back(measCap_5);
  gpibMessage_.push_back(gpibMessage_0); gpibMessage_.push_back(gpibMessage_1); gpibMessage_.push_back(gpibMessage_2); gpibMessage_.push_back(gpibMessage_3); gpibMessage_.push_back(gpibMessage_4); gpibMessage_.push_back(gpibMessage_5);
  setVLabel_.push_back(setVLabel_0); setVLabel_.push_back(setVLabel_1); setVLabel_.push_back(setVLabel_2); setVLabel_.push_back(setVLabel_3); setVLabel_.push_back(setVLabel_4); setVLabel_.push_back(setVLabel_5);
  measALabel_.push_back(measALabel_0); measALabel_.push_back(measALabel_1); measALabel_.push_back(measALabel_2); measALabel_.push_back(measALabel_3); measALabel_.push_back(measALabel_4); measALabel_.push_back(measCLabel_5);
  chanLabel_.push_back(chanLabel_0); chanLabel_.push_back(chanLabel_1); chanLabel_.push_back(chanLabel_2); chanLabel_.push_back(chanLabel_3); chanLabel_.push_back(chanLabel_4); chanLabel_.push_back(chanLabel_5);
  instrumentLabel_.push_back(instrumentLabel_0); instrumentLabel_.push_back(instrumentLabel_1); instrumentLabel_.push_back(instrumentLabel_2); instrumentLabel_.push_back(instrumentLabel_3); instrumentLabel_.push_back(instrumentLabel_4); instrumentLabel_.push_back(instrumentLabel_5);
  gpibLabel_.push_back(gpibLabel_0); gpibLabel_.push_back(gpibLabel_1); gpibLabel_.push_back(gpibLabel_2); gpibLabel_.push_back(gpibLabel_3); gpibLabel_.push_back(gpibLabel_4); gpibLabel_.push_back(gpibLabel_5);
  for(int i=0; i< maxNumInstr; i++) {
    m_ConnInstr_.push_back(NULL); // initialise connected instruments with NULL
    m_grIV_.push_back(NULL); // initialise TGraphs with NULL
    isHVsrc_.push_back(false); // initialise HVsrc with false
    isImeter_.push_back(false); // initialise HVsrc with false
  } 

  for(int i=0; i<maxNumInstr; i++){
    gpibMessage_[i]->setText("");
    //setVolt_[i]->hide();
    //measCurr_[i]->hide();
    //setVLabel_[i]->hide();
   // measALabel_[i]->hide();
   // instrumentChan_[i]->hide();
   // chanLabel_[i]->hide();
  }

  // specific for LCR meter: hide HV and V items
  checkBoxHV_5->hide();
  setVolt_5->hide();
  setVLabel_5->hide();

  extTemperBox->clear();
  extTemperBox->addItem("none");
  m_currT = -273.16;

  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
  connect(saveButtonBox, SIGNAL(clicked()), this, SLOT(saveFileDialog()));
  progressBar->hide();

}

IVscan::~IVscan(){
  //deleteIVgraphs();
  if(m_can!=NULL)          delete m_can;
  if(m_canAllXYscan!=NULL) delete m_canAllXYscan;
  //for(int i=0; i<maxNumInstr; i++) disconnDev(i);
  disconnDevAll();
}

/*void IVscan::deleteIVgraphs(){
  for(int i=0; i<maxNumInstr; i++) {
    if(m_grIV_[i]!=NULL) m_grIV_[i]->Delete();
  }
  for(uint i=0; i<m_grIVallXYscan_.size(); i++){
    if(m_grIV_[i]!=NULL) m_grIVallXYscan_[i]->Delete();
  }
  if(legAllXYscan!=NULL) legAllXYscan->Delete();
}*/

void IVscan::startScan(){

  int verbose = 0;

  // stop background meter reading thread, taken care of here
  m_thrRuns = false;
  if(m_thread.joinable()) m_thread.join();

  disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
  connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
  startButton->setText("Abort");
  extTemperBox->setEnabled(false);

  double Vstart = startVolts->value();
  double Vend   = endVolts->value();
  int nsteps = nSteps->value();
  double stepsize = (Vend-Vstart)/(double)(nsteps-1);
  int Nmeas = spinBoxNmeas->value(); // number of repeated measurements to calculate average
  double waitTimeRepetition = 10.; // waiting time before repeated measurement [ms]

  double rampVoltStep        = qRampVoltStep->value(); // [V]
  double rampVoltStepSpeed   = (double) qRampVoltStepSpeed->value(); // [ms]
  double measurementWaitTime = qMeasurementWaitTime->value(); // waiting time before each measurement after V ramp [ms]
  double rampBackToValue     = qRampBackToValue->value(); // ramp back to this value after scan

  double currentLimit = compliance->value()*1E-6;

  for(int i=0; i<maxNumInstr; i++) gpibConnectButton->setEnabled(false);
  //startButton->setEnabled(false);
  progressBar->setRange(0,nsteps);
  progressBar->show();

  //determine which instrument is HVsrc or Imeter; check if all HVsrc start from same HV value and if there are at least one HV source and one I Meter connected
  std::vector<int> idxHVsrc_;
  std::vector<int> idxImeter_;
  double initialVolt=-99999.;
  bool haveCap=false; 
  for(uint i=0; i<idxConnInstr_.size(); i++){
    //std::cout<<"Loop idx "<<i<<"; Start scan with connected instr. with idx " << idxConnInstr_[i] <<" ; HV: "<<isHVsrc_[idxConnInstr_[i]] <<"; Imeter: "<<isImeter_[idxConnInstr_[i]]<<std::endl;
    if(isHVsrc_[idxConnInstr_[i]])  {
      idxHVsrc_.push_back(idxConnInstr_[i]);
      if(gpibLabel_[idxConnInstr_[i]]->text().contains("GPIB")){
	PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxConnInstr_[i]];
	dev->measureVoltages();
	if(initialVolt==-99999) initialVolt = dev->getVoltage(0);
	else if(initialVolt != dev->getVoltage(0)){ 
	  std::cout<<"HV sources must have same initial applied voltage! ABORT scan!" <<std::endl;
	  gpibConnectButton->setEnabled(true);
	  disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
	  connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
	  startButton->setEnabled(true);
	  startButton->setText("Start IV");
	  extTemperBox->setEnabled(true);
	  progressBar->hide();
	  idxHVsrc_.clear();
	  idxImeter_.clear();
	  return;
	}
      }
    }
    if(isImeter_[idxConnInstr_[i]]){
      idxImeter_.push_back(idxConnInstr_[i]);
      if(checkBoxImeas_[idxConnInstr_[i]]->text()=="C Meter") haveCap=true;
    }
  }
  if(idxHVsrc_.size()==0 || idxImeter_.size()==0){
    std::cout<<"Must have at least one HV source and I Meter! ABORT scan!" <<std::endl;
    gpibConnectButton->setEnabled(true);
    disconnect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
    connect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue()));
    startButton->setEnabled(true);
    startButton->setText("Start IV");
    extTemperBox->setEnabled(true);
    progressBar->hide();
    idxHVsrc_.clear();
    idxImeter_.clear();
    return;
  }

  /// section to create file name and text file header
  QString fileNameQ = folderText->text();
  std::string fileName;
  std::string fileNameAllXYscan;

  if(deviceNameFolderBox->isChecked()) fileNameQ+=deviceNameText->text()+"/";
  if(deviceNameBox->isChecked()) fileNameQ+=deviceNameText->text();
  fileNameQ+="_"+fileNameBaseText->text();
  if(measIndexBox->isChecked()) fileNameQ+="_"+measIndexText->text();
  if(irradiationBox->isChecked()) fileNameQ+="_Phi"+irradiationText->text();
  if(annealingBox->isChecked()) fileNameQ+="_"+annealingText->text();
  if(userBox->isChecked()) fileNameQ+="_"+userText->text();
  if(commentBox->isChecked()) fileNameQ+="_"+commentText->text();

  if(stepX!=-1 &&stepY!=-1) {
    fileNameAllXYscan = fileNameQ.toStdString()+"_AllXYscan";
    //std::cout<<"File name All XYscan: " << fileNameAllXYscan << std::endl;
    fileNameQ+="_X"+QString::number( stepX)+"_Y"+QString::number(stepY);
  }
  fileName = fileNameQ.toStdString(); 
  //std::cout<<"File name: " << fileName << std::endl;
  std::ofstream outTxtFile;
  outTxtFile.open((fileName+".txt").c_str(), std::ofstream::out | std::ofstream::trunc);	
  //get date and time
  std::chrono::time_point<std::chrono::system_clock> startIV = std::chrono::system_clock::now();
  std::time_t scan_time = std::chrono::system_clock::to_time_t(startIV);
  std::tm* timeinfo;
  char timeChar [80];
  std::time(&scan_time);
  timeinfo = std::localtime(&scan_time);
  std::strftime(timeChar,80,"%Y-%m-%d_%H:%M",timeinfo); //custom date format

  outTxtFile << deviceNameText->text().toStdString()<<" " << fileNameBaseText->text().toStdString()<<std::endl; 
  outTxtFile << "Index: " << measIndexText->text().toStdString() << ", Irr.: " << irradiationText->text().toStdString() << ", Ann.: " << annealingText->text().toStdString( ) << ", Comments: " << commentText->text().toStdString() ;
  outTxtFile << ", Ccorr. [pF]: " << Ccorrection->value();
  if(stepX!=-1 && stepY!=-1) outTxtFile << ", Step X index: " <<  stepX << ", Rel. pos. X: " << posX << ", Step Y index: " <<  stepY << ", Relative position Y: " << posY;
  outTxtFile<< std::endl; 
  outTxtFile << "UniGoe \t" <<userText->text().toStdString()<< "\t" << timeChar <<std::endl;//std::ctime(&scan_time); 
  outTxtFile << (abs(stepsize) ? abs(stepsize) : Vstart) << "\t"<< measurementWaitTime*1e-3 << "\t" << Nmeas << "\t"; // Voltage step, delay, measurements per step,
  if(haveCap) outTxtFile << capFreqBox->currentText().toDouble()*1.e3 <<std::endl; // compliance for IV, frequency for CV
  else        outTxtFile << currentLimit <<std::endl; // compliance for IV, frequency for CV
  outTxtFile << m_ChuckT << "\t" << m_RH << std::endl; 
  outTxtFile << "t/s \t U/V";

  if(m_ChuckT==-9999 || m_RH==-9999) {
    std::cout << std::endl << "**************************" << std::endl;
    std::cout << "WARNING! No temperature or RH logging!!!"  << std::endl;
    std::cout << "**************************" << std::endl << std::endl;
  }

  m_can = (TCanvas*) gROOT->FindObject("ivcan");
  if(m_can==NULL) m_can = new TCanvas("ivcan","IV scan result",10,10,800,600);
  m_can->Clear();
  if(haveCap){
    m_can->Divide(1,2);
    m_can->cd(1);
  }
  gPad->SetRightMargin(0.3);

  // create canvas for all XY steps in case of XY scan
  if(stepX!=-1 &&stepY!=-1){
    m_canAllXYscan = (TCanvas*) gROOT->FindObject("ivcanAllXYscan");
    if(m_canAllXYscan==NULL) m_canAllXYscan = new TCanvas("ivcanAllXYscan","IV scan result all XY",810,10,800,600);
    if(stepX==1 && stepY==1) m_canAllXYscan->Clear();
    gPad->SetRightMargin(0.3);
  }

  // clean up before doing completely new IV scan (but not during XY scan)
  if((stepX==-1 && stepY==-1) || (stepX==1 && stepY==1)){
    m_grIVallXYscan_.clear();
   /* for(uint i=0; i<m_grIVallXYscan_.size(); i++){
      std::cout << "Here1 before deleting m_grIVallXYscan" << std::endl;
      if(m_grIVallXYscan_[i]!=NULL) m_grIVallXYscan_[i]->Delete();
      std::cout << "Here2 after deleting m_grIVallXYscan" << std::endl;
    }
    for(int i=0; i<maxNumInstr; i++){
      if(m_grIV_[i]!=NULL ) m_grIV_[i]->Delete();
    }*/
  }

  TGraph *oldg = (TGraph*) gROOT->FindObject("ivcurves");
  if(oldg!=NULL) oldg->Delete();
  oldg = (TGraph*) gROOT->FindObject("cvcurves");
  if(oldg!=NULL) oldg->Delete();
  oldg = NULL;
  TGraph * dummy = new TGraph(2);
  dummy->SetPoint(0,Vstart,-1);
  dummy->SetPoint(1,Vend,1);
  TGraph * dummy2 = new TGraph(2);
  dummy2->SetPoint(0,Vstart,-1);
  dummy2->SetPoint(1,Vend,1);
  if(haveCap) m_can->cd(1);
  else m_can->cd();
  dummy->Draw("AP");
  dummy->SetTitle("IV Curves");
  dummy->SetName("ivcurves");
  gROOT->GetList()->Add(dummy); // TGraphs must be added explicitly, otherwise not found later
  dummy->GetXaxis()->SetTitle("Voltage [V]");
  dummy->GetYaxis()->SetTitle("Current [A]");
  dummy->GetYaxis()->SetTitleOffset(1.1);
  if(haveCap){
    m_can->cd(2);
    dummy2->Draw("AP");
    dummy2->SetTitle("CV Curves");
    dummy2->SetName("cvcurves");
    gROOT->GetList()->Add(dummy2); // TGraphs must be added explicitly, otherwise not found later
    dummy2->GetXaxis()->SetTitle("Voltage [V]");
    dummy2->GetYaxis()->SetTitle("Capacity [pF]");
    dummy2->GetYaxis()->SetTitleOffset(1.1);
  }
  if(stepX==1 && stepY==1) {
    m_canAllXYscan->cd();
    dummy->Draw("AP");
    //if(legAllXYscan!=NULL) legAllXYscan->Delete();
    legAllXYscan = new TLegend(0.72,0.1,0.98,0.9);
    if(NScan>50) legAllXYscan->SetNColumns(2);
  }

  TLegend* leg=new TLegend(0.72,0.5,0.98,0.9);
  for(uint i=0; i<idxImeter_.size(); i++){
    std::string graphName = sensorPart_[idxImeter_[i]]->text().toStdString();
    std::stringstream a,b;
    a << stepX;
    b << stepY;
    if(stepX!=-1 &&stepY!=-1) graphName=graphName+"_X"+a.str()+"_Y"+b.str();
    m_grIV_[i] = new TGraphErrors();
    m_grIV_[i]->SetName( graphName.c_str() );
    m_grIV_[i]->SetTitle(sensorPart_[idxImeter_[i]]->text().toStdString().c_str());
    m_grIV_[i]->SetMarkerStyle(20+i);
    m_grIV_[i]->SetMarkerColor(1+i+iScan);
    m_grIV_[i]->SetLineColor(1+i+iScan);
    leg->AddEntry(m_grIV_[i], graphName.c_str(),"p");
    if(stepX!=-1 && stepY!=-1) legAllXYscan->AddEntry(m_grIV_[i], graphName.c_str(),"p");

    if(checkBoxImeas_[idxImeter_[i]]->text()=="C Meter"){
      outTxtFile <<"\t"<<sensorPart_[idxImeter_[i]]->text().toStdString()<<"Cavg/pF";
      if(Nmeas>1) outTxtFile <<"\t"<<sensorPart_[idxImeter_[i]]->text().toStdString()<<"Cstd/pF";
    } 
    else {
      outTxtFile <<"\t"<<sensorPart_[idxImeter_[i]]->text().toStdString()<<"Iavg/uA";
      if(Nmeas>1) outTxtFile <<"\t"<<sensorPart_[idxImeter_[i]]->text().toStdString()<<"Istd/uA";
    }
    //if(extTemperBox->currentText()!="none") outTxtFile << "\t "+std::string(extTemperBox->currentText().toLatin1().data())+" [C]";
  }
  outTxtFile << "\t T/C \t RH/%" <<std::endl;
  double yRangeMin = 0., yRangeMax = 0.;
  double cRangeMin = 0., cRangeMax = 0.;
  
  // start real scan
  std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentStep0 = std::chrono::high_resolution_clock::now();
  double actualVolt = initialVolt;
  for(int i=0;i<nsteps;i++){
    double volt = Vstart+stepsize*(double)i;
    double curr;

    // ramp to next voltage step
    //std::cout << "step: " <<i <<"; next voltage step: " <<volt<<"; actualVolt: " <<actualVolt<<std::endl;
    actualVolt=rampVolt(idxHVsrc_, actualVolt, volt, rampVoltStep, rampVoltStepSpeed);
    if(flagAbortScan){
      std::cout << "ABORT scan clicked! Ramp down to "<< rampBackToValue << " V and exit!" <<std::endl;
      flagAbortScan=false;
      break;
    }
    std::this_thread::sleep_for (std::chrono::milliseconds((int)measurementWaitTime));

    // measure currents
    double sum_i[10]={0.};
    double curr_[10][100]={0.};
    std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentStart = std::chrono::high_resolution_clock::now();
    if(i==0) measureCurrentStep0 = measureCurrentStart;
    outTxtFile <<  std::chrono::duration_cast<std::chrono::seconds>(measureCurrentStart - measureCurrentStep0).count();
    outTxtFile << "\t" << actualVolt;

    for(int iMeas=0; iMeas<Nmeas; iMeas++){ // measurement repetitions for average
      if(iMeas>0) std::this_thread::sleep_for (std::chrono::milliseconds((int)waitTimeRepetition));
      for(uint k=0; k<idxImeter_.size(); k++){
	curr = 0.;
	if(gpibLabel_[idxImeter_[k]]->text().contains("GPIB")){
	  PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxImeter_[k]];
	  dev->measureCurrents();
	  curr = dev->getCurrent(0);  //actual measurement
	  if(std::abs(curr) > 0.99*compliance->value()*1E-6) flagAbortScan=true; // abort if current is reaching compliance
	} else {
	  PixRs232Device* dev = (PixRs232Device*)m_ConnInstr_[idxImeter_[k]];
	  double freq = capFreqBox->currentText().toDouble()*1.e3;
	  double veff = boxCVeff->value();
          double circtype;
          QString circtypestring = capCircuitBox->currentText();
	  if(circtypestring == "Par")
		circtype=1;
	  else if(circtypestring == "Ser")
		circtype=0;
	  else circtype=2;
	  curr = dev->getCapacity(0, freq, veff, circtype, true)*1.e12 - Ccorrection->value(); //converted to pF, corrected with user defined correction
	}
	curr_[k][iMeas]=curr;
	sum_i[k]+=curr;
      }
    }
    for(uint k=0; k<idxImeter_.size(); k++){ // calculate and display measured average current
      double avg_i = sum_i[k]/Nmeas;				
      double diff_i_sqr = 0;
      for(int iMeas=0; iMeas<Nmeas; iMeas++) diff_i_sqr += std::pow((curr_[k][iMeas]-avg_i),2);
      double sigma_i = 0.;
      if(Nmeas>1) sigma_i = sqrt( diff_i_sqr/(Nmeas*(Nmeas-1)) );	//calculate std. dev. of mean
      m_grIV_[k]->SetPoint(i, volt, avg_i);
      m_grIV_[k]->SetPointError(i, 0, sigma_i);
      double conversionFactor=1; // to convert to uA
      if(checkBoxImeas_[idxImeter_[k]]->text()=="C Meter"){
	if(cRangeMin>1.2*avg_i) cRangeMin = 1.2*avg_i;
	if(cRangeMax<1.2*avg_i) cRangeMax = 1.2*avg_i;
	m_can->cd(2);
	m_grIV_[k]->Draw("PL same");
      } else {
	if(yRangeMin>1.2*avg_i) yRangeMin = 1.2*avg_i;
	if(yRangeMax<1.2*avg_i) yRangeMax = 1.2*avg_i;
	if(haveCap)
	  m_can->cd(1);
	else
	  m_can->cd();
	m_grIV_[k]->Draw("PL same");
	leg->Draw();
        conversionFactor=1e6;
      }
      outTxtFile << "\t"<<avg_i*conversionFactor;
      if(Nmeas>1) outTxtFile << "\t"<<sigma_i*conversionFactor;
      measCurr_[idxImeter_[k]]->display(avg_i);
      QApplication::processEvents(); // update GUI
    }

    std::chrono::time_point<std::chrono::high_resolution_clock> measureCurrentEnd = std::chrono::high_resolution_clock::now();
    dummy->GetYaxis()->SetRangeUser(yRangeMin, yRangeMax);
    if(haveCap) dummy2->GetYaxis()->SetRangeUser(cRangeMin, cRangeMax);
    m_can->Update();
    //if(extTemperBox->currentText()!="none") outTxtFile << "\t"<<m_currT;
    outTxtFile << "\t" << m_ChuckT << "\t" << m_RH << std::endl;
    progressBar->setValue(i);
    QApplication::processEvents();  
    
    if(flagAbortScan){
      std::cout << "Compliance reached! Ramp down to "<< rampBackToValue << " V and exit!" <<std::endl;
      flagAbortScan=false;
      break;
    }
    if(verbose){
      std::chrono::time_point<std::chrono::high_resolution_clock> endOfMeasurementPoint = std::chrono::high_resolution_clock::now();
      std::cout << "  I meas. time [ms]: " <<  std::chrono::duration_cast<std::chrono::milliseconds>(measureCurrentEnd - measureCurrentStart).count();
      std::cout << "  Update+Process [ms]: "  << std::chrono::duration_cast<std::chrono::milliseconds>(endOfMeasurementPoint - measureCurrentEnd).count() << std::endl;
    }
  }

  // draw full graph at end of each IV measurement
  std::chrono::time_point<std::chrono::high_resolution_clock> beforeDrawToAll = std::chrono::high_resolution_clock::now();
  for(uint k=0; k<idxImeter_.size(); k++){
    if(stepX!=-1 && stepY!=-1) {
          m_grIVallXYscan_.push_back(m_grIV_[k]);
          m_canAllXYscan->cd(); 
          //m_grIV_[k]->DrawClone("PL same");
          m_grIV_[k]->Draw("PL same");
          legAllXYscan->Draw();
    }
  }  
  if(verbose){
    std::chrono::time_point<std::chrono::high_resolution_clock> afterDrawToAll = std::chrono::high_resolution_clock::now();
    std::cout << "    Draw to All XY [us]: "  << std::chrono::duration_cast<std::chrono::microseconds>(afterDrawToAll - beforeDrawToAll).count() << std::endl;
  }

  // ramp voltage to desired final value at end
  rampVolt(idxHVsrc_, actualVolt, rampBackToValue, rampVoltStep, rampVoltStepSpeed/10.);

  progressBar->setValue(nsteps);

  // save data
  std::string rootFileOption = "RECREATE";
  TCanvas * canvToSave = m_can;
  std::string fileNameToSave = fileName;
  if(stepX!=-1 &&stepY!=-1) {
    canvToSave=m_canAllXYscan;
    fileNameToSave=fileNameAllXYscan;
    if(iScan!=0) rootFileOption = "UPDATE";
  }
  if(checkBoxPng->isChecked()) canvToSave->Print((fileNameToSave+".png").c_str());
  if(checkBoxTGraph->isChecked()){
    TFile rootFile((fileNameToSave+".root").c_str(),rootFileOption.c_str());
    for(uint i=0; i<idxImeter_.size(); i++) m_grIV_[i]->Write();
    rootFile.Close();
  }
  outTxtFile.close();

  gpibConnectButton->setEnabled(true);
  progressBar->hide();

  disconnect(startButton, SIGNAL(clicked()), this, SLOT(flagAbortScanToTrue() ));
  connect(startButton, SIGNAL(clicked()), this, SLOT(startScan()));
  startButton->setText("Start IV");
  extTemperBox->setEnabled(true);
  flagAbortScan = false;

  idxHVsrc_.clear();
  idxImeter_.clear();

  // re-start backround meter reading thread
  m_thrRuns = true;
  m_thread = std::thread(&IVscan::readDevThread, this);

  std::chrono::time_point<std::chrono::system_clock> endIV = std::chrono::system_clock::now();
  std::cout << "Duration IV scan [s]: "<< (std::chrono::duration_cast<std::chrono::seconds>(endIV - startIV)).count()  << std::endl;
}

double IVscan::rampVolt(std::vector<int> idxHVsrc_, double start, double end, double rampVoltStep, double rampVoltStepSpeed){

    double actualVolt = start;
    double rampUpOrDown = 0; // scan up or down?
    
    while( std::abs(actualVolt - end) >  rampVoltStep){
      if(flagAbortScan) return actualVolt; // abort if required
      rampUpOrDown = (actualVolt < end) ? 1 : -1; // scan up or down?
      actualVolt+=rampUpOrDown*rampVoltStep;
      for(uint j=0; j<idxHVsrc_.size(); j++) {
	if(gpibLabel_[idxHVsrc_[j]]->text().contains("GPIB")){
	  PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxHVsrc_[j]];
	  dev->setVoltage(0, actualVolt);
	  setVolt_[idxHVsrc_[j]]->display(actualVolt);
	  //std::cout <<"   actualVolt: " <<actualVolt<<std::endl; 
	}
      }
      if(flagAbortScan) return actualVolt; // abort if required
      QApplication::processEvents();
      std::this_thread::sleep_for (std::chrono::milliseconds((int)rampVoltStepSpeed));
      //std::cout <<"   actualVolt: " <<actualVolt<<std::endl;
    }
    for(uint j=0; j<idxHVsrc_.size(); j++) {
      actualVolt=end;
      if(gpibLabel_[idxHVsrc_[j]]->text().contains("GPIB")){
	PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[idxHVsrc_[j]];
	dev->setVoltage(0, actualVolt);
	setVolt_[idxHVsrc_[j]]->display(actualVolt);
      }
    }
    QApplication::processEvents();
    return actualVolt;
}

void IVscan::flagAbortScanToTrue(){
  flagAbortScan = true;
}

void IVscan::connDevAll() {
  bool allDevicesGood = true;
  bool nHVsrcGood = true;
  int nHVsrc = 0;
  // connect all devices selected as source or meter
  for(int i=0; i<maxNumInstr; i++) {
    int returnValue = connDev(i);
    if(isHVsrc_[i]) nHVsrc++;
    if(nHVsrc > spinBoxTotalNoHVsrc->value() ){
      nHVsrcGood=false;
      break;
    }
    if(returnValue==-1) {
      allDevicesGood=false;
      break;
    }
  }
  gpibConnectButton->setText("Disconnect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAll()));
  startButton->setEnabled(true);
  m_thrRuns = true;
  m_thread = std::thread(&IVscan::readDevThread, this);
  if(!idxConnInstr_.size()) {
    std::cout <<"No HV source or I meter connected. Try again!"<<std::endl;
    disconnDevAll();
  }
  else if(!allDevicesGood)  {
    std::cout << "Not all devices good. Aborting connection of all instruments!"<<std::endl;
    disconnDevAll();
  }
  else if(!nHVsrcGood)  {
    std::cout << "Number of HV Src checked exceeds desired total no. of HV Src. Aborting connection of all instruments!"<<std::endl;
    disconnDevAll();
  }
  else std::cout <<"Connected "<<idxConnInstr_.size()<<" instruments."<<std::endl;
}

int IVscan::connDev(int instrIdx) {

  if(checkBoxHV_[instrIdx]->isChecked())    isHVsrc_[instrIdx]  = true;
  else                                      isHVsrc_[instrIdx]  = false;
  if(checkBoxImeas_[instrIdx]->isChecked()) isImeter_[instrIdx] = true;
  else                                      isImeter_[instrIdx] = false;
  bool forceMeter = false;

  if(!isHVsrc_[instrIdx] && !isImeter_[instrIdx]) {
    // disable not connected channels
    instrumentLabel_[instrIdx]->setEnabled(false);
    sensorPart_[instrIdx]->setEnabled(false);
    checkBoxHV_[instrIdx]->setEnabled(false);
    checkBoxImeas_[instrIdx]->setEnabled(false);
    gpibLabel_[instrIdx]->setEnabled(false);
    gpibPAD_[instrIdx]->setEnabled(false);
    setVolt_[instrIdx]->setEnabled(false);
    measCurr_[instrIdx]->setEnabled(false);
    setVLabel_[instrIdx]->setEnabled(false);
    measALabel_[instrIdx]->setEnabled(false);
    instrumentChan_[instrIdx]->setEnabled(false);
    chanLabel_[instrIdx]->setEnabled(false);
    return 0; // only connect device if selected as source or meter
  }
  else if(!isHVsrc_[instrIdx] && isImeter_[instrIdx]) forceMeter = true; // if device should only act as meter (and not as source) [DOES NOT WORK FOR NOW]
  complianceLabel->setEnabled(false);
  compliance->setEnabled(false);
  totalNoHVsrcLabel->setEnabled(false);
  spinBoxTotalNoHVsrc->setEnabled(false); 

  //check if instrument has been already connected
  if(m_ConnInstr_[instrIdx]!=NULL){
    std::cout << "Instrument with index "<<instrIdx<<" already connected (NOT NULL). Disconnect first before re-connecting!!"<<std::endl;
    return 0;
  }
  for (uint i=0; i<idxConnInstr_.size(); i++){
    if(instrIdx==idxConnInstr_[i]){
    std::cout << "Instrument with index "<<instrIdx<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
    return 0;
    }
    if(gpibPAD_[instrIdx]==gpibPAD_[idxConnInstr_[i]]){
    std::cout << "Istrument with GPIB PAD "<<gpibPAD_[instrIdx]<<" already connected. Disconnect first before re-connecting!!"<<std::endl;
    return 0;
    }
  }

  double currentLimit = compliance->value()*1E-6;
  
  idxConnInstr_.push_back(instrIdx);

  if(gpibLabel_[instrIdx]->text().contains("GPIB")){

    PixGPIBDevice* thisInstr = new PixGPIBDevice(0, gpibPAD_[instrIdx]->value(), 1, forceMeter); 
  
    if(thisInstr->getDeviceFunction()==SUPPLY_HV || thisInstr->getDeviceFunction()==METER){ // check if instrument is the proper one
      m_ConnInstr_[instrIdx] = (void*)thisInstr;
      gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+
				      " with "+
				      QString::number(thisInstr->getDeviceNumberChannels())+
				      " ch.");
      instrumentChan_[instrIdx]->setValue(0);
      instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
      instrumentChan_[instrIdx]->setMinimum(0);
      
      if(thisInstr->getDeviceFunction()==SUPPLY_HV){
	thisInstr->setVoltage(0, 0.0);
	thisInstr->setCurrentLimit(0, currentLimit);
      }
      thisInstr->setState(PixGPIBDevice::PGD_ON);
      std::this_thread::sleep_for (std::chrono::milliseconds(100));
      
      gpibPAD_[instrIdx]->setEnabled(false);
      checkBoxHV_[instrIdx]->setEnabled(false);
      checkBoxImeas_[instrIdx]->setEnabled(false);
      
      setVolt_[instrIdx]->display(0.);
      measCurr_[instrIdx]->display(0.);
      
      std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription();
      if      (thisInstr->getDeviceFunction()==SUPPLY_HV) std::cout << "  Function: HV source."  << std::endl;
      else if (thisInstr->getDeviceFunction()==METER)     std::cout << "  Function: AMPERE METER."  << std::endl;
    } else {
      delete thisInstr;
      m_ConnInstr_[instrIdx] = NULL;
    }
  } else { // asume it's a RS232 device
    PixRs232Device* thisInstr = new PixRs232Device((PixRs232Device::Portids)(gpibPAD_[instrIdx]->value()-1+PixRs232Device::COM1)); 
    if(thisInstr->getDeviceFunction()==LCRMETER){ // check if instrument is the proper one
      m_ConnInstr_[instrIdx] = (void*)thisInstr;
      gpibMessage_[instrIdx]->setText("Conn. device is "+QString(thisInstr->getDescription())+
				      " with "+
				      QString::number(thisInstr->getDeviceNumberChannels())+
				      " ch.");
      instrumentChan_[instrIdx]->setValue(0);
      instrumentChan_[instrIdx]->setMaximum(thisInstr->getDeviceNumberChannels()-1);
      instrumentChan_[instrIdx]->setMinimum(0);

      gpibPAD_[instrIdx]->setEnabled(false);
      checkBoxHV_[instrIdx]->setEnabled(false);
      checkBoxImeas_[instrIdx]->setEnabled(false);
      
      setVolt_[instrIdx]->display(0.);
      measCurr_[instrIdx]->display(0.);
      
      std::cout<<"Connected instrument with index " << instrIdx <<": "<<thisInstr->getDescription()<<std::endl;
    } else {
      delete thisInstr;
      m_ConnInstr_[instrIdx] = NULL;
    }
  }
  
  if(m_ConnInstr_[instrIdx] == NULL) {
    gpibMessage_[instrIdx]->setText("ERROR connecting");
    std::cout<<"ERROR connecting instrument with index " << instrIdx <<std::endl;
    instrumentLabel_[instrIdx]->setEnabled(false);
    sensorPart_[instrIdx]->setEnabled(false);
    checkBoxHV_[instrIdx]->setEnabled(false);
    checkBoxImeas_[instrIdx]->setEnabled(false);
    gpibLabel_[instrIdx]->setEnabled(false);
    gpibPAD_[instrIdx]->setEnabled(false);
    setVolt_[instrIdx]->setEnabled(false);
    measCurr_[instrIdx]->setEnabled(false);
    setVLabel_[instrIdx]->setEnabled(false);
    measALabel_[instrIdx]->setEnabled(false);
    instrumentChan_[instrIdx]->setEnabled(false);
    chanLabel_[instrIdx]->setEnabled(false);
    return -1;
  } else
    return 1;

}

void IVscan::disconnDevAll() {
  // disconnect all devices selected as source or meter
  m_thrRuns = false;
  if(m_thread.joinable()) m_thread.join();
  for(int i=0; i<maxNumInstr; i++) disconnDev(i);
  gpibConnectButton->setText("Connect Selected");
  disconnect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(disconnDevAll()));
  connect(gpibConnectButton, SIGNAL(clicked()), this, SLOT(connDevAll()));
  startButton->setEnabled(false);
  complianceLabel->setEnabled(true);
  compliance->setEnabled(true);
  totalNoHVsrcLabel->setEnabled(true);
  spinBoxTotalNoHVsrc->setEnabled(true);
  idxConnInstr_.clear();
  isHVsrc_.clear();
  isImeter_.clear();
  std::cout<<"Disonnected all instruments." <<std::endl;
}

void IVscan::disconnDev(int instrIdx) {
  
  // enable again all devices
  instrumentLabel_[instrIdx]->setEnabled(true);
  sensorPart_[instrIdx]->setEnabled(true);
  checkBoxHV_[instrIdx]->setEnabled(true);
  checkBoxImeas_[instrIdx]->setEnabled(true);
  gpibLabel_[instrIdx]->setEnabled(true);
  gpibPAD_[instrIdx]->setEnabled(true);
  setVolt_[instrIdx]->setEnabled(true);
  measCurr_[instrIdx]->setEnabled(true);
  setVLabel_[instrIdx]->setEnabled(true);
  measALabel_[instrIdx]->setEnabled(true);
  instrumentChan_[instrIdx]->setEnabled(true);
  chanLabel_[instrIdx]->setEnabled(true);
  setVolt_[instrIdx]->display(0);
  measCurr_[instrIdx]->display(0);
  instrumentChan_[instrIdx]->setValue(0);
  instrumentChan_[instrIdx]->setMaximum(99);
  instrumentChan_[instrIdx]->setMinimum(0);  

  // return if device already not connected
  if(m_ConnInstr_[instrIdx]==NULL) return;

  if(gpibLabel_[instrIdx]->text().contains("GPIB")){
    PixGPIBDevice* dev = (PixGPIBDevice*)m_ConnInstr_[instrIdx];
    dev->setVoltage(0, 0.0);
    dev->setState(PixGPIBDevice::PGD_OFF);
    std::this_thread::sleep_for (std::chrono::milliseconds(100));  
    delete dev;
  } else {
    PixRs232Device* dev = (PixRs232Device*)m_ConnInstr_[instrIdx];
    delete dev;
  }
  m_ConnInstr_[instrIdx] = NULL;
  gpibMessage_[instrIdx]->setText("");
}

void IVscan::readDevThread() {
  while(m_thrRuns){
    readDevAll();
    QApplication::processEvents(); // update GUI
    std::this_thread::sleep_for (std::chrono::milliseconds(500));  
  }
}
void IVscan::readDevAll() {
  //std::cout<<"Devices are read." <<std::endl;
  for(uint i=0; i<idxConnInstr_.size(); i++){
    if(isHVsrc_[idxConnInstr_[i]]){
      if(gpibLabel_[idxConnInstr_[i]]->text().contains("GPIB")){
	PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[idxConnInstr_[i]];
	dev->measureVoltages();
	setVolt_[idxConnInstr_[i]]->display(dev->getVoltage(instrumentChan_[idxConnInstr_[i]]->value()));
      }
    }
    if(isImeter_[idxConnInstr_[i]]){
      if(gpibLabel_[idxConnInstr_[i]]->text().contains("GPIB")){
	PixGPIBDevice* dev = (PixGPIBDevice*) m_ConnInstr_[idxConnInstr_[i]];
	dev->measureCurrents();
	measCurr_[idxConnInstr_[i]]->display(dev->getCurrent(instrumentChan_[idxConnInstr_[i]]->value()));
      } else {
	PixRs232Device* dev = (PixRs232Device*) m_ConnInstr_[idxConnInstr_[i]];
	double freq = capFreqBox->currentText().toDouble()*1.e3;
	double veff = boxCVeff->value();
        double circtype;
        QString circtypestring = capCircuitBox->currentText();
	if(circtypestring == "Par")
		circtype=1;
	else if(circtypestring == "Ser")
		circtype=0;
	else circtype=2;
	measCurr_[idxConnInstr_[i]]->display(dev->getCapacity(instrumentChan_[idxConnInstr_[i]]->value(), freq, veff, circtype, true)*1.e12);
      }
    }
  }
}

void IVscan::saveFileDialog() {
  //fileNameLabel->setText(QFileDialog::getSaveFileName(this, tr("Select File Name"), "/work1/jlange/IVdata/IV_untitled"));
  //folderText->setText(QFileDialog::getSaveFileName(this, tr("Select File Name"), "/work1/jlange/IVdata/"));
  QFileDialog fdia(this, "Select data directory", folderText->text());
  fdia.setOption(QFileDialog::DontUseNativeDialog, true);
  fdia.setFileMode(QFileDialog::DirectoryOnly);
  if(fdia.exec() == QDialog::Accepted){
    QString path = fdia.selectedFiles().first();
    path.replace("\\", "/");
    folderText->setText(path+"/");
  }
}
void IVscan::fillTemperBox(QList<QString> tempTypes) {
  for(int i=0;i<tempTypes.size();i++)
    extTemperBox->addItem(tempTypes.at(i));
  m_currT = -273.16;
}
void IVscan::clearTemperBox(QList<QString> tempTypes) {
  for(int i=0;i<tempTypes.size();i++){
    int id = extTemperBox->findText(tempTypes.at(i));
    if(id>=0) extTemperBox->removeItem(id);
  }
  m_currT = -273.16;
}
void IVscan::fillTempVal(QMap<QString, double> Tmap){
  if(Tmap.find(extTemperBox->currentText())!=Tmap.end())
    m_currT = Tmap[extTemperBox->currentText()];
  if(Tmap.find("Chuck T(°C)")!=Tmap.end())
    m_ChuckT = Tmap["Chuck T(°C)"];
  if(Tmap.find("Ambient T(°C)")!=Tmap.end())
    m_AmbientT = Tmap["Ambient T(°C)"];
  if(Tmap.find("Rel. Humidity(%)")!=Tmap.end())
    m_RH = Tmap["Rel. Humidity(%)"];
  //std::cout << "Current T = " << m_currT << "Chuck T = " << m_ChuckT << "Amb T = " << m_AmbientT << "RH = " << m_RH << std::endl;
}
