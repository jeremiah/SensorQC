#ifndef TEMPMON_H
#define TEMPMON_H

#include "ui_Cycling.h"
#include <QWidget>
#include <thread>

class QTimer;
class simpati;

class Cycling : public QWidget, public Ui::Cycling {

    Q_OBJECT

 public:
    std::string LookUp(int i);
    void connArduino();
    int disconnArduino();
    int connDevGPIB(int instrIdx);
    void disconnDevGPIB(int instrIdx);
    int connDevAllGPIB(int instrIdx);
    void disconnDevAllGPIB(int instrIdx);
    Cycling(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~Cycling();


 public slots:
    void UpdateChainTab();
    void UpdateTempTab();
    void connDev();
    void disconnDev();
    void connDevAllGPIB();
    void disconnDevAllGPIB();
    void readMeas();
    void readTemp(std::string fileName);
    void readTempthread();
    void readChains(std::string fileName);
    void readChainsthread();
    void startCycling1();
    void startCycling2();
    void startCycling3();
    void monitorCycling1();
    void monitorCycling2();
    void monitorCycling3();
    void stopCycling();
    void endCycling();

 signals:
    void readyForMeasurement();

 private:
    QTimer *m_timer, *m_cycTimer;

    // read-out thread
    std::thread m_Chainthread;
    std::thread m_Tempthread;
    bool m_ChainthrRuns;
    bool m_TempthrRuns;
    
    int broken_counter;

    double Chain_R;
    double Temp_R;

    static const int nitems=5;
    QLCDNumber* m_setVal[nitems];
    QLCDNumber* m_measVal[nitems];
    QLabel* m_measLabels[nitems];

    simpati *m_chamber;

    //instrument parameters
    int maxNumInstr; // maximum number of instruments in GUI
    int numInstrEnabled; // number of enabled instruments
    std::vector<int> idxConnInstr_; // vector of instrument indices of connected instruments
    std::vector<void*> m_ConnInstr_; // vector of connected instruments

    std::vector<QLineEdit*> itemLabel_;
    std::vector<QSpinBox*> gpibPAD_;
    std::vector<bool> isImeter_;
    //std::vector<QPushButton*> gpibConnectButton_;
    std::vector<QSpinBox*> instrumentChan_;
    std::vector<QLCDNumber*> measRes_;
    std::vector<QLabel*> gpibMessage_;
    std::vector<QLabel*> measOhmLabel_;
    std::vector<QLabel*> chanLabel_;
    std::vector<QLabel*> instrumentLabel_;
    std::vector<QLabel*> gpibLabel_;
    std::vector<QCheckBox*> activate_checkBox_;
};

#endif // TEMPMON_H
