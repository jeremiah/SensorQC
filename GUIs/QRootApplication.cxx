//Author : Denis Bertini 01.11.2000

/**************************************************************************
* Copyright (C) 2000- Gesellschaft f. Schwerionenforschung, GSI           *
*                     Planckstr. 1, 64291 Darmstadt, Germany              *
*                     All rights reserved.                                * 
* Contact:            http://go4.gsi.de                                   * 
*                                                                         * 
* This software can be used under the license agreements as stated in     * 
* Go4License.txt file which is part of the distribution.                  *  
***************************************************************************/

// somewhat simplified to serve my needs only - JGK

#include "QRootApplication.h"
#include <TApplication.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TGraph.h>
#include <QTimer>

QRootApplication::QRootApplication(int& argc, char **argv, bool GUIenabled)
  : QApplication(argc,argv,GUIenabled), m_GUIenabled(GUIenabled){
  rootapp = 0;
  m_argv  = 0;
  if(GUIenabled){
    m_argc=argc;
    m_argv = new char*[argc];
    for(int i=0;i<argc;i++){
      m_argv[i] = new char[100];
      sprintf(m_argv[i], "%s",argv[i]);
    }
    // preventing problems with LLVM/OpenGL, see 
    // https://root-forum.cern.ch/t/error-llvm-symbols-exposed-to-cling/23597/2
    gROOT->GetInterpreter();
  }
}
QRootApplication::~QRootApplication(){
  if(rootapp!=0) rootapp->Terminate();
  delete rootapp;
  delete m_argv;
}

void QRootApplication::startTimer(){
  // connect ROOT via Timer call back
  QTimer *mainTimer = new QTimer( this );
  connect( mainTimer, SIGNAL(timeout()), this, SLOT(execute()) );
  mainTimer->start( 20 );
}   

void QRootApplication::execute(){
  // processes pending root stuff when QT is idle
  if(m_GUIenabled) {
    // create ROOT's main application if not done yet
    if(rootapp==0){
      rootapp = new TApplication("Pixel Data Viewer",&m_argc,m_argv);
    }
    gSystem->ProcessEvents();
  }
}
