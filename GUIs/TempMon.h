#ifndef TEMPMON_H
#define TEMPMON_H

#include "ui_TempMon.h"
#include <QWidget>
#include <thread>

class PixGPIBDevice;
class PixRs232Device;
class VA18B;
class QEvent;

class TempMon : public QWidget, public Ui::TempMon {

    Q_OBJECT

 public:
    TempMon(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~TempMon();

class setNewRdgEvent : public QEvent
{
 public:
  setNewRdgEvent(QMap<QString, double> vals)
    : QEvent((QEvent::Type)2011){ m_vals=vals;};
  
  QMap<QString, double> getVals(){return m_vals;};

 private:
  QMap<QString, double> m_vals;
};

    void customEvent( QEvent * e );
    double convNTC(double res, int type=0);
    double convPT1000(double res);
    double convPT100(double res);
    double convHIH4000(double measV, double nomV, double ambT);
    QString convDispl(double value);
    void readLogger(QString ipadd, int port);

 public slots:
    void connDev();
    void disconnDev();
    void readMeters();
    void typeChg(QString, uint);
    void typeChg0(QString type){typeChg(type,0);};
    void typeChg1(QString type){typeChg(type,1);};
    void typeChg2(QString type){typeChg(type,2);};
    void typeChg3(QString type){typeChg(type,3);};
    void typeChg4(QString type){typeChg(type,4);};
    void typeChg5(QString type){typeChg(type,5);};
    void typeChg6(QString type){typeChg(type,6);};
    void devSelected(bool isSel);
    void setNDUT(int);

 signals:
    void newTempDev(QList<QString>);
    void obsTempDev(QList<QString>);
    void newTempVal(QMap<QString, double>);

 private:
    QMap<QString, double> m_loggerVals;
    QMap<QString, bool> m_readLogger;
    std::vector<std::thread> m_thrDL;
    std::thread m_thread;
    bool m_thrRuns;

    static const int nitems=7;
    PixGPIBDevice *m_dev[nitems];
    PixRs232Device *m_com[nitems];
    VA18B *m_va[nitems];

    QLCDNumber* m_meterReadings[nitems];
    QCheckBox* m_checkBox[nitems];
    QLabel* m_gpibLabels[nitems];
    QLabel* m_unitLabels[nitems];
    QSpinBox* m_gpibPADs[nitems];
    QComboBox* m_gpibComboBox[nitems];
    QComboBox* m_calibBox[nitems];
    QLineEdit* m_ipAdds[nitems];
    QLabel* m_chanLabels[nitems];
    QSpinBox* m_devChans[nitems];
};

#endif // TEMPMON_H
