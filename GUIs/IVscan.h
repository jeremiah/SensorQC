#ifndef IVSCAN_H
#define IVSCAN_H

#include "ui_IVscan.h"
#include <QWidget>
#include <QSignalMapper>
#include <thread>
#include <vector>

class TCanvas;
class TGraph;
class TGraphErrors;
class TLegend;
class PixGPIBDevice;

class IVscan : public QWidget, public Ui::IVscan {

    Q_OBJECT

 public:
  IVscan(QWidget * parent = 0, Qt::WindowFlags flags = Qt::Window);
  ~IVscan();
  int connDev(int instrIdx);
  void disconnDev(int instrIdx);
  double rampVolt(std::vector<int> idxHVsrc_, double start, double end, double rampVoltStep, double rampVoltStepSpeed);
  int stepX;
  int stepY;
  double posX;
  double posY;
  int iScan;
  int NScan;
  //void deleteIVgraphs();

 public slots:
   void startScan();
   void connDevAll();
   void disconnDevAll();
   void readDevAll();
   void readDevThread();
   void saveFileDialog();
   void flagAbortScanToTrue();
   void fillTemperBox(QList<QString> tempTypes);
   void clearTemperBox(QList<QString> tempTypes);
   void fillTempVal(QMap<QString, double>);
 private:
   TCanvas *m_can;
   TCanvas *m_canAllXYscan;
   TLegend* legAllXYscan;
   std::vector<TGraphErrors*> m_grIV_;
   std::vector<TGraphErrors*> m_grIVallXYscan_;
   std::vector<void*> m_ConnInstr_; // vector of connected instruments
   bool flagAbortScan;
   double m_ChuckT;
   double m_AmbientT;
   double m_RH;
   double m_currT;

   // read-out thread
   std::thread m_thread;
   bool m_thrRuns;

   //instrument parameters
   int maxNumInstr; // maximum number of instruments in GUI
   int numInstrEnabled; // number of enabled instruments
   std::vector<int> idxConnInstr_; // vector of instrument indices of connected instruments

   std::vector<QLineEdit*> sensorPart_;
   std::vector<QCheckBox*> checkBoxHV_;
   std::vector<bool> isHVsrc_;
   std::vector<QCheckBox*> checkBoxImeas_;
   std::vector<bool> isImeter_;
   std::vector<QSpinBox*> gpibPAD_;
   //std::vector<QPushButton*> gpibConnectButton_;
   std::vector<QSpinBox*> instrumentChan_;
   std::vector<QLCDNumber*> setVolt_;
   std::vector<QLCDNumber*> measCurr_;
   std::vector<QLabel*> gpibMessage_;
   std::vector<QLabel*> setVLabel_;
   std::vector<QLabel*> measALabel_;
   std::vector<QLabel*> chanLabel_;
   std::vector<QLabel*> instrumentLabel_;
   std::vector<QLabel*> gpibLabel_;
  
   

};

#endif // IVSCAN_H
