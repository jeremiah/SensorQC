#include "TempMon.h"
#include "DataLogging.h"
#include "QRootApplication.h"
#include <QMainWindow>
#include <QDockWidget>

#include <iostream>
#include <sstream>
#include <exception>

int main( int argc, char** argv )
{

  // start root and QT application
  QRootApplication app( argc, argv);

  // create main data viewer window
  QMainWindow *Win = new QMainWindow(0);
  Win->setWindowTitle("Temperature Monitoring");

  QDockWidget* dockWidget_1 = new QDockWidget(Win);
  dockWidget_1->setObjectName(QStringLiteral("dockWidget_1"));
  dockWidget_1->setWindowTitle("Temperature Devices");
  QDockWidget* dockWidget_2 = new QDockWidget(Win);
  dockWidget_2->setObjectName(QStringLiteral("dockWidget_2"));
  dockWidget_2->setWindowTitle("Data Logging");

  TempMon *tm = new TempMon(Win, Qt::Window);
  dockWidget_1->setWidget(tm);
  DataLogging *dl = new DataLogging(Win, Qt::Window);
  dockWidget_2->setWidget(dl);
  QWidget::connect(tm, SIGNAL(newTempVal(QMap<QString, double>)), dl, SLOT(newValues(QMap<QString, double>)) );
  QWidget::connect(tm, SIGNAL(obsTempDev(QList<QString>)), dl, SLOT(obsValues(QList<QString>)) );

  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_1);
  Win->addDockWidget(Qt::LeftDockWidgetArea,  dockWidget_2);
  Win->show();

  // executing our application
  int ret = 0;
  std::stringstream msg;
  try{
    app.startTimer();
    ret  = app.exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;
  // cleaning up
  delete Win;

  return ret;
}
