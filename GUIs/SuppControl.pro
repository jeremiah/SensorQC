TEMPLATE = app

CONFIG += qt
equals(QT_MAJOR_VERSION, 5) {
  QT+= widgets
}
unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

INCLUDEPATH += . ../PixGPIB ../PixRS232 ../include

FORMS += SuppControlBase.ui \
	 SuppControl.ui

SOURCES += SuppControl.cxx \
	   SuppControlMain.cxx \
	   SuppControlBase.cxx \
	   QRootApplication.cxx

HEADERS += SuppControl.h \
	   SuppControlBase.h \
	   QRootApplication.h

unix {
    DESTDIR =  .
	QMAKE_CXXFLAGS += -fPIC -DCF__LINUX -DHAVE_GPIB -DUSE_LINUX_GPIB
	QMAKE_LFLAGS += -lgpib -lpthread
        INCLUDEPATH += $${system(root-config --incdir)}
        QMAKE_LFLAGS  +=  $${system(root-config --libs)} -L../PixGPIB -lPixGPIB -L../PixRS232 -lPixRS232
        QMAKE_RPATHDIR += ../PixGPIB ../PixRS232
}

win32 {
    DESTDIR =  ../bin
    DEFINES += WIN32 
    DEFINES += _WINDOWS
    DEFINES += _MBCS 
    QMAKE_CXXFLAGS += -MP
    QMAKE_CXXFLAGS += -MD
    INCLUDEPATH += $(ROOTSYS)/include
    QMAKE_LFLAGS_RELEASE = delayimp.lib
    QMAKE_LFLAGS_WINDOWS += /LIBPATH:../PixGPIB /LIBPATH:../bin /LIBPATH:$(ROOTSYS)/lib
    LIBS += PixGPIB.lib GPIB-32.obj
    LIBS += PixRS232.lib
    LIBS += libCore.lib
    LIBS += libCint.lib 
    LIBS += libRIO.lib 
    LIBS += libNet.lib 
    LIBS += libHist.lib 
    LIBS += libGraf.lib 
    LIBS += libGraf3d.lib 
    LIBS += libGpad.lib
    LIBS += libTree.lib
    LIBS += libRint.lib
    LIBS += libPostscript.lib
    LIBS += libMatrix.lib
    LIBS += libPhysics.lib
    LIBS += libMathCore.lib
}

