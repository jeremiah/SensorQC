TEMPLATE = lib

unix {
  GCC_VERSION = $$system("g++ --version | grep g++")
  contains(GCC_VERSION, [5-9].[0-9].[0-9]) {
     CONFIG += c++14
  } else {
     contains(GCC_VERSION, 4.[8-9].[0-9]) {
       CONFIG += c++11
     } else {
       message( "unknown g++ version: ")
       message($$GCC_VERSION)
     }
  }
} else {
  CONFIG += c++11
}
CONFIG -= qt
CONFIG -= debug
CONFIG -= debug_and_release
CONFIG += release

SOURCES = PixRs232Device.cxx \
	  VA18B.cxx

INCLUDEPATH += . ../include

win32 {
  DESTDIR = ../bin
  DLLDESTDIR = ../bin
  SOURCES += ComTools.cxx
  DEFINES += WIN32 PIX_DLL_EXPORT
}

unix {
  DESTDIR = .
  DEFINES += CF__LINUX
  SOURCES += ComToolsLinux.cxx
}

