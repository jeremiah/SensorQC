#include "VA18B.h"
#include "ComTools.h"

#include <string.h>
#include <thread>
#include <map>
#include <bitset>

VA18B::VA18B(int iPort) : m_iPort(iPort+10){
  ComOpen(m_iPort, 2400);
  std::this_thread::sleep_for (std::chrono::milliseconds(200));  
  // skip first three readings, might be littered with old data
  char buffer[65];
  for(int i=0;i<3;i++){
    if(ComGetReadCount(m_iPort)>0)
      ComRead(m_iPort, buffer, 64);
    std::this_thread::sleep_for (std::chrono::milliseconds(100));  
  }

  m_runMeasLoop = true;
  m_measThr = std::thread(&VA18B::readFromMeter, this);
}
VA18B::~VA18B(){
  m_runMeasLoop = false;
  if(m_measThr.joinable()) m_measThr.join();

  ComClose(m_iPort);
}
void VA18B::readFromMeter(){
  const int bLen = 64;
  const unsigned int maxChuncks=16;
  char buffer[bLen+1];
  m_nFailedReads = 0;
  const int maxNFailedReads = 10;
  m_measVal = 0.;
  m_measUnit = "unknown";

  std::string allbits="";
  for(unsigned int i=0;i<maxChuncks;i++) allbits +="0000";

  while(m_runMeasLoop){
    buffer[0] = '\0';
    if(ComGetReadCount(m_iPort)>0){
      int nb = ComRead(m_iPort, buffer, bLen);
      buffer[nb] = '\0';
    }
    if(strlen(buffer)>0){
      for(unsigned int i=0;i<maxChuncks;i++){
	std::bitset<4>  b ((unsigned int)(buffer[i]&0xf));
	int ipart = ((buffer[i]>>4)&0xf)-1;
	//printf("%d - %d\n", i, ipart);
	if(ipart>=0 && ipart<(int)maxChuncks)
	  allbits.replace(ipart*4,4,b.to_string());
      }
      //printf("%s\n", allbits.c_str());
      std::map<std::string, std::string> digimap;
      digimap["1111101"] = "0";
      digimap["0000101"] = "1";
      digimap["1011011"] = "2";
      digimap["0011111"] = "3";
      digimap["0100111"] = "4";
      digimap["0111110"] = "5";
      digimap["1111110"] = "6";
      digimap["0010101"] = "7";
      digimap["1111111"] = "8";
      digimap["0111111"] = "9";

      std::string d1 = digimap[allbits.substr(5,7)];
      std::string d2 = digimap[allbits.substr(13,7)];
      std::string d3 = digimap[allbits.substr(21,7)];
      std::string d4 = digimap[allbits.substr(29,7)];
      std::string val = ((allbits.substr(4,1)=="1")?"-":"") + d1 +
	((allbits.substr(12,1)=="1")?".":"") + d2 +
	((allbits.substr(20,1)=="1")?".":"") + d3 +
	((allbits.substr(28,1)=="1")?".":"") + d4;
      double dval = (double)atof(val.c_str());
      std::string unit="";
      if(allbits.substr(36,1)=="1")      dval *= 1.e-6;
      else if(allbits.substr(37,1)=="1") dval *= 1.e-9;
      else if(allbits.substr(38,1)=="1") dval *= 1.e3;
      else if(allbits.substr(40,1)=="1") dval *= 1.e-3;
      else if(allbits.substr(42,1)=="1") dval *= 1.e6;

      unit += ((allbits.substr(44,1)=="1")?"F":"");
      unit += ((allbits.substr(45,1)=="1")?"Ohm":"");
      unit += ((allbits.substr(48,1)=="1")?"A":"");
      unit += ((allbits.substr(49,1)=="1")?"V":"");
      unit += ((allbits.substr(50,1)=="1")?"Hz":"");
      unit += ((allbits.substr(54,1)=="1")?"°C":"");
      //printf("%.2lf %s\n", dval, unit.c_str());
      m_measVal = dval;
      m_measUnit = unit;
      m_nFailedReads = 0;
    } else
      m_nFailedReads++;

    if(m_nFailedReads>maxNFailedReads){
      m_measVal = 0.;
      m_measUnit = "unknown";
    }

    std::this_thread::sleep_for (std::chrono::milliseconds(250));  
  }
}
